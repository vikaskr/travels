<?php

$date = Yii::$app->getRequest()->getQueryParam('datepick');
$pick = Yii::$app->getRequest()->getQueryParam('pick');
$drop = Yii::$app->getRequest()->getQueryParam('drop');


if($pick == 'haridwar' || $pick == 'kashipur' || $pick == 'rudrapur' || $pick == 'bareilly' || $pick == 'shahjahanpur'
|| $pick == 'sitapur' || $pick == 'lucknow')
{
   Yii::$app->response->redirect(array('site/results', 'pick'=> $pick, 'drop'=> $drop, 'datepick'=> $date));
}

else
{
   Yii::$app->response->redirect(array('site/result', 'pick'=> $pick, 'drop'=> $drop, 'datepick'=> $date));
}

?>