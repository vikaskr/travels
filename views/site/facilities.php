<style type="text/css">
	.fleet-3-cols .fleet-block {
    float: left;
    width: calc(33%);
    margin: 0px!important; 
}
.center {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 50%;
}
</style>
<style type="text/css">
	@media screen and (max-width: 980px){
.full-width, .three-fourth, .one-half, .one-third, .two-third, .one-fourth, .one-fifth, .one-sixth {
    width: 100%;
}}
</style>
<br>

	<main class="main" role="main" style="padding: 0 0 10px;">
		<header class="site-title color">
			<div class="wrap">
				<div class="container">
					<h1>Facilities</h1>
					<nav role="navigation" class="breadcrumbs">
						<ul>
							<li><a href="/" title="Home">Home</a></li>
							<li>Facilities</li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		<div class="wrap">
			<div class="row">
				<!--- Content -->
				<div class="content three-fourth textongrey">
					<h2>SKT Travels</h2>
					<p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia.</p> 
					<div class="one-third" style="padding: 0px;">
						<img src="/images/image2.png" alt="" style="width: 100px; padding-top: 0px; padding-bottom: 10px;" class="center" align="middle">
						<h4 style="text-align: center!important; margin-bottom: 0px; margin-top: 0px;"><a href="#"><strong>Free wi-fi</strong></a></h4>
					</div>
					<div class="one-third" style="padding: 0px;">
						<img src="/images/image3.png" alt="" style="width: 100px; padding-top: 0px; padding-bottom: 10px;" class="center" align="middle">
						<h4 style="text-align: center!important; margin-bottom: 0px; margin-top: 0px;"><a href="#"><strong>Dinner & Water</strong></a></h4>
					</div>
					<div class="one-third" style="padding: 0px;">
						<img src="/images/image4.png" alt="" style="width: 100px; padding-top: 0px; padding-bottom: 10px;" class="center" align="middle">
						<h4 style="text-align: center!important; margin-bottom: 0px; margin-top: 0px;"><a href="#"><strong>Soft Drinks</strong></a></h4>
					</div>
					<div class="one-third" style="padding: 0px;">
						<img src="/images/image5.png" alt="" style="width: 100px; padding-top: 0px; padding-bottom: 10px;" class="center" align="middle">
						<h4 style="text-align: center!important; margin-bottom: 0px; margin-top: 0px;"><a href="#"><strong>Fully AC</strong></a></h4>
					</div>
					<div class="one-third" style="padding: 0px;">
						<img src="/images/image6.png" alt="" style="width: 100px; padding-top: 0px; padding-bottom: 10px;" class="center" align="middle">
						<h4 style="text-align: center!important; margin-bottom: 0px; margin-top: 0px;"><a href="#"><strong>Sleeper Class Seats</strong></a></h4>
					</div>
					<div class="one-third" style="padding: 0px;">
						<img src="/images/image1.png" alt="" style="width: 100px; padding-top: 0px; padding-bottom: 10px;" class="center" align="middle">
						<h4 style="text-align: center!important; margin-bottom: 0px; margin-top: 0px;"><a href="#"><strong>Inbuild Washroom</strong></a></h4>
					</div>
					
				</div>
				<!--- //Content -->
				
				<!--- Sidebar -->
				<aside class="one-fourth sidebar right offset">
					<!-- Widget -->
					<div class="widget">
						<h4>Why book with us?</h4>
						<div class="textwidget">
							<h5>Reliable and Safe</h5>
							<p>Lorem ipsum dolor sit amet,  do eiusmod tempor incididunt labore et dolore magna aliqua.</p>
							<h5>No hidden fees</h5>
							<p>Lorem ipsum dolor sit amet,  do eiusmod tempor incididunt labore et dolore magna aliqua.</p>
							<h5>We’re Always Here</h5>
							<p>Lorem ipsum dolor sit amet,  do eiusmod tempor incididunt labore et dolore magna aliqua.</p>
						</div>
					</div>
					<!-- //Widget -->
					
				</aside>
				<!--- //Sidebar -->
			</div>
		</div>
	</main>

				

		