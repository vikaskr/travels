<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\ClientSignup;
$model = new ClientSignup();
?>
<style type="text/css">
    .product-advertise-form-inner input{
        padding: 15px 10px!important;
    }
    .wid{
        width: 204px!important; 
        padding: 7px!important;
         margin-top: 10px!important;
    }
    .modal-open .modal{
        padding-top: 41em!important;
        margin-top: 6em!important;
        background: none!important;
    }
    .product-advertise-form-inner input {
    padding: 8px 8px!important;
}
</style>

<button type="button" class="close" onClick="closeContact();" data-dismiss="modal">&times;</button>
    <div class="login-popup-outer">
    <h3 class="magazines-by-sectors-heading" style="margin-bottom: 0px!important;"><strong>REGISTER</strong></h3>
       <?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'validationUrl' => ['site/client-register-validate'],'options' => ['id' => 'frontend-signup','method' => 'POST','data-pjax' => true],'action' => ['/#']]); ?>
            <div class="col-xs-12 nopadding product-advertise-form">

                <div class="col-xs-12 nopadding product-advertise-form-inner">
                    <div class="row">
                        <div class="col-md-6">
                        <?= $form->field($model, 'first_name')->textInput(['placeholder' => 'First Name', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>  
                        <div class="col-md-6">
                        <?= $form->field($model, 'last_name')->textInput(['placeholder' => 'Last Name', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 nopadding product-advertise-form-inner">
                    <div class="row">
                        <div class="col-md-6">
                        <?= $form->field($model, 'username')->textInput(['placeholder' => 'Username', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>  
                        <div class="col-md-6">
                        <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                    </div>
                </div>

                 <div class="col-xs-12 nopadding product-advertise-form-inner">
                    <div class="row">
                        <div class="col-md-6">
                        <?= $form->field($model, 'mobile')->textInput(['placeholder' => 'Mobile', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                        <div class="col-md-6">
                        <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                    </div>   
                </div>

                <div class="col-xs-12 nopadding product-advertise-form-inner">
                    <div class="row">
                        <div class="col-md-12">
                        <?= $form->field($model, 'address')->textInput(['placeholder' => 'Address', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                    </div>   
                </div>

                <div class="col-xs-12 nopadding product-advertise-form-inner">
                    <div class="row">
                        <div class="col-md-6">
                        <?= $form->field($model, 'pin_code')->textInput(['placeholder' => 'Pincode/Zipcode', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                        <div class="col-md-6">
                        <?= $form->field($model, 'city')->textInput(['placeholder' => 'City', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                    </div>   
                </div>
                <div class="col-xs-12 nopadding product-advertise-form-inner">
                    <div class="row">
                        <div class="col-md-6">
                        <?= $form->field($model, 'state')->textInput(['placeholder' => 'State', 'class' => 'login-popup-panel-main input'])->label(false);?>
                        </div>
                        <div class="col-md-6">
                        <?= $form->field($model, 'country')->dropDownList(['Afghanistan','Albania','Algeria','Andorra','Angola','Antigua and Barbuda','Argentina','Armenia','Aruba','Australia','Austria','Azerbaijan','Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bhutan', 'Bolivia',' Bosnia and Herzegovina', 'Botswana', 'Brazil', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burma', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cabo Verde', 'Central African', 'Republic', 'Chad', 'Chile', 'China', 'Colombia', 'Comoros', 'Congo Democratic Republic of the', 'Congo Republic of the','Costa Rica', 'Cote d Ivoire', 'Croatia', 'Cuba', 'Curacao', 'Cyprus','Czechia','Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Fiji','Finland', 'France', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Greece', 'Grenada', 'Guatemala','Guinea','Guinea-Bissau','Guyana','Haiti', 'Holy See', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Israel','Italy','Jamaica', 'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Kosovo', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg','Macau', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Mali','Malta', 'Marshall Islands', 'Mauritania', 'Mauritius', 'Mexico', 'Micronesia', 'Moldova', 'Monaco', 'Mongolia', 'Montenegro', 'Morocco', 'Mozambique','Namibia', 'Nauru', 'Nepal', 'Netherlands', 'New Zealand', 'Nicaragua','Niger','Nigeria', 'North Korea', 'Norway','Oman', 'Pakistan', 'Palau', 'Palestinian Territories', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines','Poland', 'Portugal', 'Qatar', 'Romania', 'Russia', 'Rwanda', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Vincent and the Grenadines', 'Samoa', 'San Marino', 'Sao Tome and Principe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Sint Maarten', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'South Korea', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Timor-Leste', 'Togo', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Tuvalu', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States of America','Uruguay', 'Uzbekistan', 'Vanuatu', 'Venezuela', 'Vietnam', 'Yemen', 'Zambia','Zimbabwe',], ['prompt'=>'Select Country','class' => 'wid'])->label(false);?>
                        </div>
                    </div>   
                </div>

                <div class="col-xs-12 nopadding product-advertise-form-inner product-submit-button">
                    <input type="submit" name="Register" value="Register">
                </div>
                <div class="col-xs-12 nopadding product-advertise-form-inner memnber-yet-con"><span>Already have a Account?  <a href="#" class="signup-user" id="signin-modal">Login</a></span></div>
            </div>
        <?php ActiveForm::end(); ?>
</div>


