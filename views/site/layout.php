<?php
$seat = new \app\modules\MubAdmin\modules\yoga\models\Course();
$booked = new \app\modules\MubAdmin\modules\yoga\models\Booked();
$sleeperPrice = new \app\modules\MubAdmin\modules\yoga\models\Category();
$price = $sleeperPrice::find()->where(['category_name' => 'Sleeper','del_status' => '0'])->one();
$price2 = $sleeperPrice::find()->where(['category_name' => 'Chair','del_status' => '0'])->one();

$date = Yii::$app->getRequest()->getQueryParam('date');
$pick = Yii::$app->getRequest()->getQueryParam('pick');
$drop = Yii::$app->getRequest()->getQueryParam('drop');

$datepick = strtotime($date);

$book = $booked::find()->where(['date_booked' => $date, 'froms' => 'cd'])->all();
?>
<?php 
foreach($book as $value){
$seat = $value['seat'];
}?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style type="text/css">
.asiento label:after {
  filter: alpha(opacity=0);
  opacity: 0;
  content: '';
  position: absolute;
  width: 46px;
  height: 72px;
  background: #00bf00;
  background: linear-gradient(top, #0895d3 0%, #0966a8 100%);
  top: 0px;
  left: 0px;
  box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0, 0, 0, 0.5);
}

.asiento label:hover::after {
  filter: alpha(opacity=30);
  opacity: 0.3;
}

.asiento input[type=checkbox]:checked + label:after {
  filter: alpha(opacity=100);
  opacity: 1;
}
.asientoo label:after {
  filter: alpha(opacity=0);
  opacity: 0;
  content: '';
  position: absolute;
  width: 46px;
  height: 43px;
  background: #00bf00;
  background: linear-gradient(top, #0895d3 0%, #0966a8 100%);
  top: 0px;
  left: 0px;
  box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0, 0, 0, 0.5);
}
.asientoo{
	background: #fff;
}
.asiento{
	background: #fff;
}
.asientoo label:hover::after {
  filter: alpha(opacity=30);
  opacity: 0.3;
}

.asientoo input[type=checkbox]:checked + label:after {
  filter: alpha(opacity=100);
  opacity: 1;
}
label {
    display: inline-block;
    max-width: 100%;
    margin-left: -5px!important;
    margin-bottom: 5px;
    font-weight: 700;
}

body {
    font-family:  Verdana,sans-serif;
    }
</style>
 <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> 
<br/><br/><div id="page-header">
	<h1>Bus Details</h1>
	<div class="title-block3"></div>
</div>
		<?php
	$items = array();
	foreach ($book as $value) {
	$items[] = $value['seat'];
	}
    ?>
	<div class="container">
		<div class="row" style="padding-left: 30px;">
			 <h4><a href="#"><strong>Garibrath Select Your Seats</strong></a></h4>
			<div class="col-md-3">
				<button style="border: 1px solid #000!important; background: #fff; padding: 10px;"></button>&nbsp;&nbsp;<span>Available </span>&nbsp;&nbsp;&nbsp;&nbsp;<br/><button style="padding: 10px; border: 1px solid #000!important; background: #cd1010;"></button>&nbsp;&nbsp;<span><span>Unavailable</span><br/><br/><br/>
			</div>
			<div class="col-md-3">
		    Price<strong> ₹ <?= $price2->price;?> (Chair)</strong><br/>
			Price<strong> ₹ <?= $price->price;?> (Sleeper)</strong>
			<br/><br/><br/>
			</div>
			<div class="col-md-3">
			</div>
		</div>
	</div>
	<div class="container">
	 <form method="GET" action="/site/form" id="seat">
		<div class="row">
			<div class="col-md-3" style=" padding: 10px;  background: #f0f0f0; margin-left: 20px;">
				<div class="row">
					<button style="width: 40%; margin-left: 20px; background: #030f79; color: #fff; border: none; padding: 5px;">Toilet</button><br/><br/>
					<p align="center" style="margin: 0 0 15px 0!important;"><strong>Chair &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sleeper</strong></p>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('c1', $items))) {
                            ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="chair1" value="chair1" name="chair1" />
								<label for="chair1">C1</label>
							</div>
							 <?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
					          <?php } ?>
					          <?php if (!(in_array('c2', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="chair2" value="chair2" name="chair2" />
								<label for="chair2">C2</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
					          <?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('c3', $items))) {
                            ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="chair3" value="chair3" name="chair3" />
								<label for="chair3">C3</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
					          <?php } ?>
					          <?php if (!(in_array('c4', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-left: 5px;margin-top: 3px;">
								<input type="checkbox" id="chair4" value="chair4" name="chair4" />
								<label for="chair4">C4</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>

							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('c5', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="chair5" value="chair5" name="chair5" />
								<label for="chair5">C5</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>
					          <?php if (!(in_array('c6', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-left: 5px;margin-top: 3px;">
								<input type="checkbox" id="chair6" value="chair6" name="chair6" />
								<label for="chair6">C6</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							 <?php if (!(in_array('c7', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="chair7" value="chair7" name="chair7" />
								<label for="chair7">C7</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>
							 <?php if (!(in_array('c8', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-left: 5px;margin-top: 3px;">
								<input type="checkbox" id="chair8" value="chair8" name="chair8" />
								<label for="chair8">C8</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>

							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							 <?php if (!(in_array('c9', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="chair9" value="chair9" name="chair9" />
								<label for="chair9">C9</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>
							 <?php if (!(in_array('c10', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-left: 5px;margin-top: 3px;">
								<input type="checkbox" id="chair10" value="chair10" name="chair10" />
								<label for="chair10">C10</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>

							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('c11', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="chair11" value="chair11" name="chair11" />
								<label for="chair11">C11</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>
							 <?php if (!(in_array('c12', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-left: 5px;margin-top: 3px;">
								<input type="checkbox" id="chair12" value="chair12" name="chair12" />
								<label for="chair12">C12</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('c13', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="chair13" value="chair13" name="chair13" />
								<label for="chair13">C13</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>
							 <?php if (!(in_array('c14', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-left: 5px;margin-top: 3px;">
								<input type="checkbox" id="chair14" value="chair14" name="chair14" />
								<label for="chair14">C14</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('c15', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="chair15" value="chair15" name="chair15" />
								<label for="chair15">C15</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>
							 <?php if (!(in_array('c16', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asientoo" style="width: 46px; height: 45px; padding-top: 5px; margin-left: 5px;margin-top: 3px;">
								<input type="checkbox" id="chair16" value="chair16" name="chair16" />
								<label for="chair16">C16</label>
							</div>
							<?php } else {?>
					       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							</div>
							<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s37', $items))) {
                              ?>
								<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; margin-top: 3px;">
									<input type="checkbox" id="sleeper37" value="sleeper37" name="sleeper37" />
								    <label for="sleeper37">S37</label>
								</div>
								<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							 <?php if (!(in_array('s38', $items))) {
                              ?>
								<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px;margin-left: 5px; margin-top: 3px;">
									<input type="checkbox" id="sleeper38" value="sleeper38" name="sleeper38" />
								    <label for="sleeper38">S38</label>
								</div>
								<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
								<div class="col-md-2"></div>
						</div>

					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s25', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper25" value="sleeper25" name="sleeper25" />
								<label for="sleeper25">S25</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s26', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper26" value="sleeper26" name="sleeper26" />
								<label for="sleeper26">S26</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s27', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper27" value="sleeper27" name="sleeper27" />
								<label for="sleeper27">S27</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s28', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper28" value="sleeper28" name="sleeper28" />
								<label for="sleeper28">S28</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s29', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper29" value="sleeper29" name="sleeper29" />
								<label for="sleeper29">S29</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s30', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper30" value="sleeper30" name="sleeper30" />
								<label for="sleeper30">S30</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s31', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper31" value="sleeper31" name="sleeper31" />
								<label for="sleeper31">S31</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s32', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper32" value="sleeper32" name="sleeper32" />
								<label for="sleeper32">S32</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s33', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper33" value="sleeper33" name="sleeper33" />
								<label for="sleeper33">S33</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s34', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper34" value="sleeper34" name="sleeper34" />
								<label for="sleeper34">S34</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s35', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper35" value="sleeper35" name="sleeper35" />
								<label for="sleeper35">S35</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s36', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleepers36" value="sleepers36" name="sleepers36" />
								<label for="sleepers36">S36</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>

					</div>
			    </div>
	        </div>

	        <div class="col-md-3" style=" padding: 10px;  background: #f0f0f0; margin-left: 20px;">
				<div class="row">
					<p align="center" style="margin: 0 0 15px 0!important;"><strong>Start</strong></p>
					<p align="center" style="margin: 0 0 27px 0!important;"><strong>Sleeper</strong></p>

					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s1', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper1" value="sleeper1" name="sleeper1" />
								<label for="sleeper1">S1</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s2', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper2" value="sleeper2" name="sleeper2" />
								<label for="sleeper2">S2</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s5', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper5" value="sleeper5" name="sleeper5" />
								<label for="sleeper5">S5</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s6', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px;margin-top: 3px;">
								<input type="checkbox" id="sleeper6" value="sleeper6" name="sleeper6" />
								<label for="sleeper6">S6</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s9', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper9" value="sleeper9" name="sleeper9" />
								<label for="sleeper9">S9</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s10', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px;margin-top: 3px;">
								<input type="checkbox" id="sleeper10" value="sleeper10" name="sleeper10" />
								<label for="sleeper10">S10</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s13', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper13" value="sleeper13" name="sleeper13" />
								<label for="sleeper13">S13</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s14', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px;margin-top: 3px;">
								<input type="checkbox" id="sleeper14" value="sleeper14" name="sleeper14" />
								<label for="sleeper14">S14</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
					
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s17', $items))) {
                              ?>
								<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; margin-top: 3px;">
									<input type="checkbox" id="sleeper17" value="sleeper17" name="sleeper17" />
								    <label for="sleeper17">S17</label>
								</div>
								<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s18', $items))) {
                              ?>
								<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px;margin-left: 5px; margin-top: 3px;">
									<input type="checkbox" id="sleeper18" value="sleeper18" name="sleeper18" />
								    <label for="sleeper18">S18</label>
								</div>
								<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
								<div class="col-md-2"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s21', $items))) {
                              ?>
								<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; margin-top: 3px;">
									<input type="checkbox" id="sleeper21" value="sleeper21" name="sleeper21" />
								    <label for="sleeper21">S21</label>
								</div>
								<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s22', $items))) {
                              ?>
								<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px;margin-left: 5px; margin-top: 3px;">
									<input type="checkbox" id="sleeper22" value="sleeper22" name="sleeper22" />
								    <label for="sleeper22">S22</label>
								</div>
								<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
								<div class="col-md-2"></div>
						</div>

					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s3', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper3" value="sleeper3" name="sleeper3" />
								<label for="sleeper3">S3</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s4', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper4" value="sleeper4" name="sleeper4" />
								<label for="sleeper4">S4</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s7', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper7" value="sleeper7" name="sleeper7" />
								<label for="sleeper7">S7</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s8', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper8" value="sleeper8" name="sleeper8" />
								<label for="sleeper8">S8</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s11', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper11" value="sleeper11" name="sleeper11" />
								<label for="sleeper11">S11</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s12', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper12" value="sleeper12" name="sleeper12" />
								<label for="sleeper12">S12</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s15', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper15" value="sleeper15" name="sleeper15" />
								<label for="sleeper15">S15</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s16', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper16" value="sleeper16" name="sleeper16" />
								<label for="sleeper16">S16</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s19', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper19" value="sleeper19" name="sleeper19" />
								<label for="sleeper19">S19</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s20', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper20" value="sleeper20" name="sleeper20" />
								<label for="sleeper20">S20</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>
						<div class="row">
							<div class="col-md-1 col-xs-1"></div>
							<?php if (!(in_array('s23', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper23" value="sleeper23" name="sleeper23" />
								<label for="sleeper23">S23</label>
							</div>
							<?php } else {?>
					          <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
							 </div>
							<?php } ?>
							<?php if (!(in_array('s24', $items))) {
                              ?>
							<div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 74px; padding-top: 5px; margin-left: 5px; margin-top: 3px;">
								<input type="checkbox" id="sleeper24" value="sleeper24" name="sleeper24" />
								<label for="sleeper24">s24</label>
							</div>
							<?php } else {?>
							       <div class="col-md-2 col-xs-2 asiento" style="width: 46px; height: 45px; padding-top: 5px; margin-top: 3px; background: #cd1010!important;" onclick="myFunction()">
									</div>
								<?php } ?>
							<div class="col-md-1 col-xs-1"></div>
						</div>

					</div>
			    </div>
	        </div>

			
			
			<div class="col-md-4" style="margin-left: 50px;">
				<div class="fleet-block">
					<div class="fleet-block-content"><br/>
						<h4><strong>Bus Route</strong></h4>
						<p>Rohini Sector-11, Vazirabad, Sahdara, Simapuri Border, Mohan Nagar Ghaziabad, Vasundhra, Noida Sector-62, Mamora Choak, Noida Sector-71, City center Noida-37, Mahamaya Flyover, Kushinagar, Gopalgunj, Mirgunj, Siwan, Gopalpur, Hasanpura, Chainpur, Rasulpur, Ekma.
						</p><br/>
					 <input type="hidden" name="pick" value="<?= $pick;?>">
					 <input type="hidden" name="drop" value="<?= $drop;?>">
					 <input type="hidden" name="date" value="<?= $date;?>">
		            <input type="submit" name="submit" value="Proceed" style="margin-top: 2em; width: 100px; padding:12px; background:#cd1010; border: none; border-radius: 20px; margin-left: 2em; font-size: 16px;color: #fff;">
					</div>
				</div>
			</div>

		</div>
		<div class="row" style="margin-top: 5px;">
			<div class="col-md-5"></div>
		</div>
	</form>
	</div>
<br/><br/>
<div class="content-wrapper-outer clearfix">
</div>
<script>
function myFunction() {
    alert("Seats Booked! Choose Others");
}
</script>