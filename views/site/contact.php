<?php
if(isset($_GET['name']))
    {
        $name = @trim(stripslashes($_GET['name'])); 
        $email = @trim(stripslashes($_GET['email'])); 
        $phone = @trim(stripslashes($_GET['phone'])); 
        $subj = @trim(stripslashes($_GET['subject'])); 
        $to      = 'vikkumar648@gmail.com';
        $subject = 'Contact From SKT Travels';
        $body = "Name: " . $name . "\n" . "Email: " . $email . "\n" . "Phone: " . $phone . "\n". "Subject: " . $subj . "\n";
        $headers = 'From: info@seac.co.in' . "\r\n" .
            'Reply-To: info@skt.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $user = "$email";
        $usersubject = "Thank You Contact With SKT travels";
        $userheaders = 'From: info@seac.co.in' . "\r\n" ;
        $usermessage = "Thank you for filling our contact form.";
        mail($to,$subject,$body,$headers);
        mail($user,$usersubject,$usermessage,$userheaders);
    }
?>
<style type="text/css">
	@media screen and (max-width: 980px){
.full-width, .three-fourth, .one-half, .one-third, .two-third, .one-fourth, .one-fifth, .one-sixth {
    width: 100%;
}}
</style>
<main class="main" role="main">
		
		<header class="site-title color">
			<div class="wrap">
				<div class="container">
					<h1>Contact us</h1>
					<nav role="navigation" class="breadcrumbs">
						<ul>
							<li><a href="/" title="Home">Home</a></li>
							<li>Contact</li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		
		<div class="wrap">
			<div class="row">
				
				<div class="full-width content textongrey">
					<h2>Send us a message</h2>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit. All fields are required.</p>
				</div>
				
				<div class="three-fourth">
					<form method="GET" action="" name="form">
						<div id="message"></div>
						<div class="f-row">
							<div class="one-half">
								<label for="name">Name </label>
								<input type="text" name="name" id="name">
							</div>
							<div class="one-half">
								<label for="email">Email </label>
								<input type="email" name="email" id="email"><br/>
							</div>
							<div class="one-half">
								<label for="phone">Phone </label>
								<input type="text" name="phone" id="phone">
							</div>
							<div class="one-half">
								<label for="subject">Subject </label>
								<input type="text" name="subject" id="subject" style="width: 95%;">
							</div>
						</div>
						
						<div class="f-row"><br/>
							<input type="submit" value="Submit" id="submit" name="submit" class="btn color medium" style="width: 20%;">
						</div>
					</form>
				</div>
				
				<aside class="one-fourth sidebar right">
					<!-- Widget -->
					<div class="widget">
						<h4>Need help booking?</h4>
						<div class="textwidget">
							<p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your needs.</p>
							<p class="contact-data"><span class="icon icon-themeenergy_call black"></span> +1 555 555 555</p>
						</div>
					</div>
					
				</aside>
				
			</div>
		</div>
	</main>