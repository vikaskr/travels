<?php
$seat = new \app\modules\MubAdmin\modules\yoga\models\Course();
$booked = new \app\modules\MubAdmin\modules\yoga\models\Booked();
$sleeperPrice = new \app\modules\MubAdmin\modules\yoga\models\Category();
$price = $sleeperPrice::find()->where(['category_name' => 'Sleeper','del_status' => '0'])->one();
$price2 = $sleeperPrice::find()->where(['category_name' => 'Chair','del_status' => '0'])->one();

$date = Yii::$app->getRequest()->getQueryParam('date');
$pick = Yii::$app->getRequest()->getQueryParam('pick');
$drop = Yii::$app->getRequest()->getQueryParam('drop');

$datepick = strtotime($date);

$book = $booked::find()->where(['date_booked' => $date, 'froms' => 'dc'])->all();

?>
<?php 
foreach($book as $value){
$seat = $value['seat'];
}?>
<style type="text/css">

input[type=checkbox] {
  visibility: hidden;
}

.containerbus {
 /* display: flex;*/
  justify-content: center;
}

.autobus {
  padding-top: 15px;
    background: lightgray;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: 200px;
    min-height: 230px;
    padding-bottom: 15px;
}

.fila {
  display: flex;
  justify-content: space-between;
}

.seccion {
  display: flex;
  width: 40%;
  justify-content: space-between;
}

.asiento {
  width: 30px;
  height: 30px;
  color: white;
  font-size: 10;
  text-align: center;
  background: #fcfff4;
  background: linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
  margin: 5px auto;
  box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0, 0, 0, 0.5);
  position: relative;
}

.asiento label {
  cursor: pointer;
    font-size: 14px!important;
  position: absolute;
  padding-top: 5px;
  color: #000;
  width: 24px;
  height: 24px;
  left: 3px;
  top: 3px;
  /*box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.5), 0px 1px 0px rgba(255, 255, 255, 1);*/
  background: linear-gradient(top, #222 0%, #45484d 100%);
}

.asiento label:after {
  filter: alpha(opacity=0);
  opacity: 0;
  content: '';
  position: absolute;
  width: 24px;
  height: 24px;
  background: #00bf00;
  background: linear-gradient(top, #0895d3 0%, #0966a8 100%);
  top: 0px;
  left: 0px;
  box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0, 0, 0, 0.5);
}

.asiento label:hover::after {
  filter: alpha(opacity=30);
  opacity: 0.3;
}

.asiento input[type=checkbox]:checked + label:after {
  filter: alpha(opacity=100);
  opacity: 1;
}
.fleet-2-cols .fleet-block {
    float: left;
    background: #fff!important;
}
</style>
<br/><br/><div id="page-header">
	<h1>Bus Details</h1>
	<div class="title-block3"></div>
	
</div>
		
	<?php
	$items = array();
	foreach ($book as $value) {
	$items[] = $value['seat'];
	}
    ?>

<!-- BEGIN .content-wrapper-outer -->
<div class="content-wrapper-outer clearfix">
	
	<!-- BEGIN .main-content -->
<div class="main-content main-content-full">
  <div class="fleet-block-wrapper fleet-1-cols clearfix">
	<div class="fleet-block">
		<div class="fleet-block-content"><br/>

		    <h4><a href="#"><strong>Garibrath Select Your Seats</strong></a></h4>
			<div class="title-block3"></div>
			<div class="fleet-block-wrapper fleet-3-cols clearfix" style="border: none!important;">
				<div class="fleet-block" style="border: none!important;">
				<div class="fleet-block-content" style="border-top: none!important;">
					<button style="border: 1px solid #000!important; background: #fff;"></button>&nbsp;&nbsp;<span>Available </span>&nbsp;&nbsp;&nbsp;&nbsp;<br/><button style="border: 1px solid #000!important; background: #cd1010;"></button>&nbsp;&nbsp;<span><span>Unavailable</span><br/><br/><br/>
					<h4><strong>Lower Birth</strong></h4>
	              <form method="GET" action="/site/form" id="seat">
					<div class="containerbus">
					  <!-- Squared ONE -->
					  <div class="autobus">
					  	<p align="center" style="margin: 0 0 15px 0!important;"><strong>Start</strong></p>
					  	<button style="width: 40%;">Toilet</button><br/>
					  	<p align="center" style="margin: 0 0 15px 0!important;"><strong><span style="text-align: left;">Chair</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Sleeper</span></strong></p>

					    <div class="fila">
					      <div class="seccion">
					       <?php if (!(in_array('c1', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair1" value="chair1 , " name="chair1" />
					          <label for="chair1">c1</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         <?php if (!(in_array('c2', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair2" value="chair2 , " name="chair2" />
					          <label for="chair2">c2</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					       
					      </div>
					      <div class="seccion">
					       
					       <?php if (!(in_array('s25', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper25" value="Sleeper25 , " name="Sleeper25" />
					          <label for="Sleeper25">s25</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					          <?php if (!(in_array('s26', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper26" value="Sleeper26 , " name="Sleeper26" />
					          <label for="Sleeper26">s26</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         
					      </div>
					    </div>

					    <div class="fila">
					      <div class="seccion">
					        
					       <?php if (!(in_array('c3', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair3" value="chair3 , " name="chair3" />
					          <label for="chair3">c3</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					      
					        <?php if (!(in_array('c4', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair4" value="chair4 , " name="chair4" />
					          <label for="chair4">c4</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					       
					        
					      </div>
					      <div class="seccion">
					        
					        <?php if (!(in_array('s27', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper27" value="Sleeper27 , " name="Sleeper27" />
					          <label for="Sleeper27">s27</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					       <?php if (!(in_array('s28', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper28" value="Sleeper28 , " name="Sleeper28" />
					          <label for="Sleeper28">s28</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        
					      </div>
					    </div>

					    <div class="fila">
					      <div class="seccion">
					       
					       <?php if (!(in_array('c5', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair5" value="chair5 , " name="chair5" />
					          <label for="chair5">c5</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         <?php if (!(in_array('c6', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair6" value="chair6 , " name="chair6" />
					          <label for="chair6">c6</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					       
					      </div>
					      <div class="seccion">
					      
					       <?php if (!(in_array('s29', $items))) {
                            ?>
					       <div class="asiento">
					          <input type="checkbox" id="Sleeper29" value="Sleeper29 , " name="Sleeper29" />
					          <label for="Sleeper29">s29</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         <?php if (!(in_array('s30', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper30" value="Sleeper30 , " name="Sleeper30" />
					          <label for="Sleeper30">s30</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>

					        
					      </div>
					    </div>

					    <div class="fila">
					      <div class="seccion">
					         
					          <?php if (!(in_array('c7', $items))) {
                            ?>
					         <div class="asiento">
					          <input type="checkbox" id="chair7" ,  value="chair7" name="chair7" />
					          <label for="chair7">c7</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					          <?php if (!(in_array('c8', $items))) {
                            ?>
					         <div class="asiento">
					          <input type="checkbox" id="chair8" value="chair8 , " name="chair8" />
					          <label for="chair8">c8</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					      </div>
					      <div class="seccion">
					         
					   <?php if (!(in_array('s31', $items))) {
                            ?>
					         <div class="asiento">
					          <input type="checkbox" id="Sleeper31" value="Sleeper31 , " name="Sleeper31" />
					          <label for="Sleeper31">s31</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        <?php if (!(in_array('s32', $items))) {
                            ?>
					         <div class="asiento">
					          <input type="checkbox" id="Sleeper32" value="Sleeper32 , " name="Sleeper32" />
					          <label for="Sleeper32">s32</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        
					      </div>
					    </div>

					    <div class="fila">
					      <div class="seccion">
					      	 
					      	<?php if (!(in_array('c9', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair9" value="chair9 , " name="chair9" />
					          <label for="chair9">c9</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					       <?php if (!(in_array('s10', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair10" value="chair10 , " name="chair10" />
					          <label for="chair10">c10</label>
					        </div>
					         
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					      </div>
					      <div class="seccion">
					      	 
					      	<?php if (!(in_array('s33', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper33" value="Sleeper33 , " name="Sleeper33" />
					          <label for="Sleeper33">s33</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        <?php if (!(in_array('s34', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper34" value="Sleeper34 , " name="Sleeper34" />
					          <label for="Sleeper34">s34</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         
					      </div>
					    </div>

					    <div class="fila">
					      <div class="seccion">
					        
					        <?php if (!(in_array('c11', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair11" value="chair11 , " name="chair11" />
					          <label for="chair11">c11</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					          <?php if (!(in_array('c12', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair12" value="chair12 , " name="chair12" />
					          <label for="chair12">c12</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        
					      </div>
					      <div class="seccion">
					        
					        <?php if (!(in_array('s35', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper35" value="Sleeper35 , " name="Sleeper35" />
					          <label for="Sleeper35">s35</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        <?php if (!(in_array('s36', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper36" value="Sleeper36 , " name="Sleeper36" />
					          <label for="Sleeper36">s36</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         
					      </div>
					    </div>
					    <div class="fila">
					      <div class="seccion">
					       
					       <?php if (!(in_array('c13', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair13" value="chair13 , " name="chair13" />
					          <label for="chair13">c13</label>
					        </div>
					         <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         <?php if (!(in_array('c14', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair14" value="chair14 , " name="chair14" />
					          <label for="chair14">c14</label>
					        </div>
					         <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					       
					      </div>
					    </div>
					    <div class="fila">
					      <div class="seccion">
					       
					     <?php if (!(in_array('c15', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair15" value="chair15 , " name="chair15" />
					          <label for="chair15">c15</label>
					        </div>
					         <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					       <?php if (!(in_array('c16', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair16" value="chair16 , " name="chair16" />
					          <label for="chair16">c16</label>
					        </div>
					         <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        
					      </div>
					    </div>
					    <div class="fila">
					      <div class="seccion">
					       
					     <?php if (!(in_array('c37', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair37" value="chair37 , " name="chair37" />
					          <label for="chair37">c37</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					       
					      <?php if (!(in_array('c38', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="chair38" value="chair38 , " name="chair38" />
					          <label for="chair38">c38</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        
					      </div>
					    </div>

					  </div>
					</div>
			</div>
	</div>
	
	<div class="fleet-block" style="border: none!important;">
		<div class="fleet-block-content" style="border-top: none!important;">
			Price<strong> ₹ <?= $price2->price;?> (Chair)</strong><br/><br/>
			Price<strong> ₹ <?= $price->price;?> (Sleeper)</strong>
			<br/><br/><br/>
			<h4><strong>Upper Birth</strong></h4>
				
					<div class="containerbus">
					  <!-- Squared ONE -->
					  <div class="autobus">
					  	<p align="center" style="margin: 0 0 15px 0!important;"><strong>Start</strong></p>
					  	<p align="center" style="margin: 0 0 15px 0!important;"><strong>Sleeper</strong></p>
					    <div class="fila">
					      <div class="seccion">
					      	
					        <?php if (!(in_array('s1', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper1" value="Sleeper1 , " name="Sleeper1" />
					          <label for="Sleeper1">s1</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        <?php if (!(in_array('s2', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper2" value="Sleeper2 , " name="Sleeper2" />
					          <label for="Sleeper2">s2</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					      </div>

					      <div class="seccion">
					      	 
					      	 <?php if (!(in_array('s3', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper3" value="Sleeper3 , " name="Sleeper3" />
					          <label for="Sleeper3">s3</label>
					        </div>
					          <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         
					      	 <?php if (!(in_array('s4', $items))) {
                            ?>
					       
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper4" value="Sleeper4 , " name="Sleeper4" />
					          <label for="Sleeper4">s4</label>
					        </div>
					          <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					     
					      </div>
					    </div>

					    <div class="fila">
					      <div class="seccion">
					       <?php if (!(in_array('s5', $items))) {
                            ?>
					        
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper5" value="Sleeper5 , " name="Sleeper5" />
					          <label for="Sleeper5">s5</label>
					        </div>
					         
					         <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        <?php if (!(in_array('s6', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper6" value="Sleeper6 , " name="Sleeper6" />
					          <label for="Sleeper6">s6</label>
					        </div>
					         <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					      </div>
					      <div class="seccion">
					      	
					      	  <?php if (!(in_array('s7', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper7" value="Sleeper7 , " name="Sleeper7" />
					          <label for="Sleeper7">s7</label>
					        </div>
					         <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         <?php if (!(in_array('s8', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper8" value="Sleeper8 , " name="Sleeper8" />
					          <label for="Sleeper8">s8</label>
					        </div>
					         <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        
					      </div>
					    </div>

					    <div class="fila">
					      <div class="seccion">
					      	
					      	  <?php if (!(in_array('s9', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper9" value="Sleeper9 , " name="Sleeper9" />
					          <label for="Sleeper9">s9</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					          <?php if (!(in_array('s10', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper10" value="Sleeper10 , " name="Sleeper10" />
					          <label for="Sleeper10">s10</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         
					      </div>
					      <div class="seccion">
					      	
					       <?php if (!(in_array('s11', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper11" value="Sleeper11 , " name="Sleeper11" />
					          <label for="Sleeper11">s11</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					          <?php if (!(in_array('s12', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper12" value="Sleeper12 , " name="Sleeper12" />
					          <label for="Sleeper12">s12</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         
					      </div>
					    </div>

					    <div class="fila">
					      <div class="seccion">
					        
					        <?php if (!(in_array('s13', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper13" value="Sleeper13 , " name="Sleeper13" />
					          <label for="Sleeper13">s13</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					         <?php if (!(in_array('s14', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper14" value="Sleeper14 , " name="Sleeper14" />
					          <label for="Sleeper14">s14</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        
					      </div>
					      <div class="seccion">
					        
					       <?php if (!(in_array('s15', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper15" value="Sleeper15 , " name="Sleeper15" />
					          <label for="Sleeper15">s15</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>

					          <?php if (!(in_array('s16', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper16" value="Sleeper16 , " name="Sleeper16" />
					          <label for="Sleeper16">s16</label>
					        </div>
					      	<?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					       
					      </div>
					    </div>

					    <div class="fila">
					      <div class="seccion">
					        
					       <?php if (!(in_array('s17', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper17" value="Sleeper17 , " name="Sleeper17" />
					          <label for="Sleeper17">s17</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        
					       <?php if (!(in_array('s18', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper18" value="Sleeper18 , " name="Sleeper18" />
					          <label for="Sleeper18">s18</label>
					        </div>
					         <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					      </div>
					      <div class="seccion">
					        
					        <?php if (!(in_array('s19', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper19" value="Sleeper19 , " name="Sleeper19" />
					          <label for="Sleeper19">s19</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					       <?php if (!(in_array('s20', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper20" value="Sleeper20 , " name="Sleeper20" />
					          <label for="Sleeper20">s20</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					      </div>
					    </div>

					    <div class="fila">
					      <div class="seccion">
					      	
					      	<?php if (!(in_array('s21', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper21" value="Sleeper21 , " name="Sleeper21" />
					          <label for="Sleeper21">s21</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        <?php if (!(in_array('s22', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper22" value="Sleeper22 , " name="Sleeper22" />
					          <label for="Sleeper22">s22</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        
					      </div>
					      <div class="seccion">
					      	
					      	<?php if (!(in_array('s23', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper23" value="Sleeper23 , " name="Sleeper23" />
					          <label for="Sleeper23">s23</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        <?php if (!(in_array('s24', $items))) {
                            ?>
					        <div class="asiento">
					          <input type="checkbox" id="Sleeper24" value="Sleeper24 , " name="Sleeper24" />
					          <label for="Sleeper24">s24</label>
					        </div>
					        <?php } else {?>
					        <div class="asiento" style="background: red;" onclick="myFunction()">
					          
					        </div>
					          <?php } ?>
					        
					      </div>
					    </div>

					  </div>
					</div><br/>
					 <input type="hidden" name="pick" value="<?= $pick;?>">
					 <input type="hidden" name="drop" value="<?= $drop;?>">
					 <input type="hidden" name="date" value="<?= $date;?>">
					  <input type="submit" name="submit" value="Proceed" style="margin-top: 2em; padding:12px; background:#cd1010; border: none; border-radius: 20px; margin-left: 2em; font-size: 16px;color: #fff;">
					 </form>
						</div>
					</div>
					<div class="fleet-block">
						<div class="fleet-block-content"><br/>
							<h5><strong>Bus Route</strong></h5>
							<p>Rohini Sector-11, Vazirabad, Sahdara, Simapuri Border, Mohan Nagar Ghaziabad, Vasundhra,
								Noida Sector-62, Mamora Choak, Noida Sector-71, City center Noida-37, Mahamaya Flyover,
								Pari Choak, Agra, Lucknow, Faizabad, Ayodhya, Gorakhpur, Kushinagar, Gopalgunj, Mirgunj, Siwan, Gopalpur, Hasanpura, Chainpur, Rasulpur, Ekma.
							</p>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="content-wrapper-outer clearfix">

	
</div>
<script>
function myFunction() {
    alert("Seats Booked! Choose Others");
}
</script>