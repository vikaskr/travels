<style type="text/css">
	@media screen and (max-width: 980px){
.full-width, .three-fourth, .one-half, .one-third, .two-third, .one-fourth, .one-fifth, .one-sixth {
    width: 100%;
}}
</style>
<main class="main" role="main">

		<header class="site-title color">
			<div class="wrap">
				<div class="container">
					<h1>Gallery</h1>
					<nav role="navigation" class="breadcrumbs">
						<ul>
							<li><a href="/" title="Home">Home</a></li>
							<li>Gallery</li>
						</ul>
					</nav>
				</div>
			</div>
		</header>

		
		<div class="wrap">
			<div class="row">
				<div class="full-width content textongrey">
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit. All fields are required.</p>
				</div>
			</div>
		</div>
	</main>