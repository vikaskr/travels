<?php 

$fname = Yii::$app->getRequest()->getQueryParam('name');
$date = Yii::$app->getRequest()->getQueryParam('date');
$pick = Yii::$app->getRequest()->getQueryParam('pickup');
$drop = Yii::$app->getRequest()->getQueryParam('drop');
$phone = Yii::$app->getRequest()->getQueryParam('phone');
$email = Yii::$app->getRequest()->getQueryParam('email');
$address = Yii::$app->getRequest()->getQueryParam('address');
$seat = Yii::$app->getRequest()->getQueryParam('seats');
$pin_code = Yii::$app->getRequest()->getQueryParam('pincode');
$price = Yii::$app->getRequest()->getQueryParam('price');


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "nouf";
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "INSERT INTO contact_mail (name, pick, dropoff, book_date, phone, price, seat, email, subject)
VALUES ('$fname', '$pick', '$drop', '$date', '$phone', '$price', '$seat', '$email', '$address')";

if ($conn->query($sql) === TRUE) {
    echo "";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

?>
<style type="text/css">
	@media screen and (max-width: 980px){
.full-width, .three-fourth, .one-half, .one-third, .two-third, .one-fourth, .one-fifth, .one-sixth {
    width: 100%;
}}
</style>
<br/> <br/>
<main class="main" role="main" style="padding: 0 0 10px;">

<div class="services">
  <div class="container"> 
	<div class="row">    
	<h1>Payment Process</h1><br/>
	<div class="wrap">
		<div class="row">
			<div class="three-fourth">
				<div class="box readonly">
					<h3>Passenger Details</h3>
					<div class="f-row">
						<div class="one-fourth">Name</div>
						<div class="three-fourth"><b><?= $fname;?></b></div>
					</div>
					<div class="f-row">
						<div class="one-fourth">Mobile number</div>
						<div class="three-fourth"><b><?= $phone;?></b></div>
					</div>
					<div class="f-row">
						<div class="one-fourth">Email address</div>
						<div class="three-fourth"><b><?= $email;?></b></div>
					</div>
					<div class="f-row">
						<div class="one-fourth">Pickup Location</div>
						<div class="three-fourth"><b><?= $pick;?></b></div>
					</div>
					<div class="f-row">
						<div class="one-fourth">Drop Location</div>
						<div class="three-fourth"><b><?= $drop;?></b></div>
					</div>
					<div class="f-row">
						<div class="one-fourth">Pincode</div>
						<div class="three-fourth"><b><?= $pin_code;?></b></div>
					</div>
					<div class="f-row">
						<div class="one-fourth">Address</div>
						<div class="three-fourth"><b><?= $address;?></b></div>
					</div>
					<div class="f-row">
						<div class="one-fourth">Booked Seat</div>
						<div class="three-fourth"><b><?= $seat;?></b></div>
					</div>
					<div class="f-row">
						<div class="one-fourth">Journey Date</div>
						<div class="three-fourth"><b><?= $date;?></b></div>
					</div>
					<div class="f-row">
						<div class="one-fourth">Total Amount</div>
						<div class="three-fourth"><b><?= $price;?></b></div>
					</div>
					<div>

					<form method="post" action="/Non_Seamless_kit/ccavRequestHandler.php">
						<?php
						 $mechentId = "194502";
						?>
						<input type="hidden" name="txn_id" id="txn_id" value="<?= $fname.$price;?>" />
    
				        <input type="hidden" id="merchant_id" name="merchant_id" value="<?= $mechentId;?>"/>
				        
				        <input type="hidden" id="order_id" name="order_id" value="<?= $seat;?>"/>
				        
				        <input type="hidden" id="amount" name="amount" value="<?= $price;?>"/>
				        
				        <input type="hidden" id="redirect_url" name="redirect_url" value="<?='http://'.$_SERVER['SERVER_NAME'].'/components/success.php' ;?>"/>
				        
				        <input type="hidden" id="cancel_url" name="cancel_url" value="<?='http://'.$_SERVER['SERVER_NAME'].'/components/failure.php?name=$fname;';?>"/>
				        
				        <input type="hidden" id="language" name="language" value="EN"/>
				          
				        <input type="hidden" id="fname" name="billing_name" value="<?= $fname?>"/>

				        <input type="hidden" id="last_name" name="last_name" value="India"/>

				        <input type="hidden" id="ticket" name="ticket" value="Bus Ticket"/>

				        <input type="hidden" id="pin_code" name="billing_zip" value="<?= $pin_code;?>"/>
				           
				        <input type="hidden" id="address" name="billing_address" value="<?= $pick.' '.$date;?>"/>
				            
				        <input type="hidden" id="city" name="billing_city" value="<?= $pick;?>"/>
				                       
				        <input type="hidden" id="state" name="billing_state" value="<?= $pick;?>"/>

				        <input type="hidden" id="country" name="billing_country" value="India"/>

				        <input type="hidden" id="currency" name="currency" value="INR"/>
				             
				        <input type="hidden" id="phone1" name="billing_tel" value="<?= $phone;?>"/>

				        <input type="hidden" id="landline" name="landline" value="2154"/>
				           
				        <input type="hidden" id="email" name="billing_email" value="<?= $email;?>"/>           
				           
				        <input type="hidden" id="delivery_name" name="delivery_name" value="<?= $fname;?>"/>

				        <input type="hidden" id="delivery_address" name="delivery_address" value="<?= $drop;?>"/>
				           
			            <input type="hidden" id="delivery_city" name="delivery_city" value="<?= $drop;?>"/>
			            
			            <input type="hidden" id="delivery_state" name="delivery_state" value="<?= $drop;?>"/>

			            <input type="hidden" id="delivery_country" name="delivery_country" value="India"/>
			            
			            <input type="hidden" id="delivery_tel" name="delivery_tel" value="<?= $phone;?>"/>
				            
				        <input type="hidden" id="merchant_param1" name="merchant_param1" value="additional Info."/>
			           
			            <input type="hidden" id="merchant_param2" name="merchant_param2" value="additional Info."/>
			       
			            <input type="hidden" id="merchant_param3" name="merchant_param3" value="additional Info."/>
			           
			            <input type="hidden" id="merchant_param4" name="merchant_param4" value="additional Info."/>
			           
			            <input type="hidden" id="" name="merchant_param5" value="additional Info."/>
			        
			            <input type="hidden" id="promo_code" name="promo_code" value=""/>
			        
			            <input type="hidden" id="customer_identifier" name="customer_identifier" value=""/>
				        
			            <div class="contact-subject-field">
			            <button type="submit" class="btn btn-success" style="margin-top: 20px;" onclick="disable()">
		 				 Proceed to Checkout 
					    </button>
			            </div>

					</form>
				</div>
			</div>
		</div>
			
		<aside class="one-fourth sidebar right">
			<!-- Widget -->
			<div class="widget">
				<h4>Need help booking?</h4>
				<div class="textwidget">
					<p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your needs.</p>
					<p class="contact-data"><span class="icon icon-themeenergy_call black"></span> +1 555 555 555</p>
				</div>
			</div>

		</aside>

		</div>
	</div>
</main>
<script type="text/javascript">
	function disable(){
  document.getElementById('formSave').setAttribute("disabled", "disabled"); 
}
</script>