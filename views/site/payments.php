<?php 
$fname = Yii::$app->getRequest()->getQueryParam('first-name');
$lname = Yii::$app->getRequest()->getQueryParam('last-name');
$date = Yii::$app->getRequest()->getQueryParam('date');

$pick = Yii::$app->getRequest()->getQueryParam('pick');
$drop = Yii::$app->getRequest()->getQueryParam('drop');
$phone1 = Yii::$app->getRequest()->getQueryParam('phone1');
$phone2 = Yii::$app->getRequest()->getQueryParam('phone2');
$email = Yii::$app->getRequest()->getQueryParam('email');
$seat = Yii::$app->getRequest()->getQueryParam('seat');
$price = Yii::$app->getRequest()->getQueryParam('price');


?>
		<br/> <br/>
		<div id="page-header">
			<h1>Booking Confirm</h1>
			<div class="title-block3"></div>
			
		</div>
		
		<!-- BEGIN .content-wrapper-outer -->
		<div class="content-wrapper-outer clearfix">
			
			<!-- BEGIN .main-content -->
			<div class="main-content main-content-full">
				
				<!-- BEGIN .booking-step-wrapper -->

				
				<!-- BEGIN .full-booking-wrapper -->
				<div class="full-booking-wrapper full-booking-wrapper-3 clearfix" style="background: #423b3b!important;">
					
					<h4>Your Booking Confirm</h4>
					<div class="title-block7"></div>
					
					<!-- BEGIN .clearfix -->
					<div class="clearfix">
						
						<!-- BEGIN .qns-one-half -->
						<div class="qns-one-half">
						
							<p class="clearfix"><strong>Service:</strong> <span><?= $pick;?> To <?= $drop;?></span></p>
							<p class="clearfix"><strong>From:</strong> <span><?= $pick?></span></p>
							<p class="clearfix"><strong>To:</strong> <span><?= $drop;?></span></p>
							<p class="clearfix"><strong>Route Estimate:</strong> Rohini Sector-11, Vazirabad, Sahdara, Simapuri Border, Mohan Nagar Ghaziabad, Vasundhra,
								Noida Sector-62, Mamora Choak, Noida Sector-71, City center Noida-37, Mahamaya Flyover,
								Pari Choak, Agra, Lucknow, Faizabad, Ayodhya, Gorakhpur, Kushinagar, Gopalgunj, Mirgunj, Siwan, Gopalpur, Hasanpura, Chainpur, Rasulpur, Ekma.<span></p>
						
						<!-- END .qns-one-half -->
						</div>
						
						<!-- BEGIN .qns-one-half -->
						<div class="qns-one-half last-col">
						
							<p class="clearfix"><strong>Date:</strong> <span><?= $date;?></span></p>
							<p class="clearfix"><strong>Seats</strong> <span><?= $seat;?></span></p>
							<p class="clearfix"><strong>Pick Up Time:</strong> <span>4:00 PM</span></p>
							<p class="clearfix"><strong>Vehicle:</strong> <span>Luxury Bus</span></p>
							<p class="clearfix"><strong>Price:</strong> <span><?= $price;?></span></p>
						
						<!-- END .qns-one-half -->
						</div>
						
					<!-- END .clearfix -->
					</div>
					
					<hr class="space2">
					
					
						
					<!-- END .clearfix -->
					</div>
					
				<!-- END .full-booking-wrapper -->
				</div>
				
			<!-- END .main-content -->
			</div>
		
		<!-- END .content-wrapper-outer -->
		</div>