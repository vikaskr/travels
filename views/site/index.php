<style type="text/css">
	@media screen and (max-width: 980px){
.one-fourth{
    width: 100%;
}}
</style>
<main class="main" role="main" style="padding: 0px!important;">
<!-- Intro -->
	<div class="intro" style="background: url(./images/banner.jpg); width: 100%; height: 530px; display: block; background-repeat:no-repeat; background-position:center;">
		<div class="wrap">
			<div class="textwidget">
			  <div class="advanced-search" id="booking" style="background-color: #225378ab;">
			    <div class="wrap">
				  <form role="form" method="get" action="/site/redirect">
					<!-- Row -->
					<div class="f-row">

						<div class="form-group select one-fourth">
							<div>
								<select name="pick" required>
									<option value="">From</option>
									<option value="haridwar">Haridwar</option>
									<option value="kashipur">Kashipur</option>
									<option value="rudrapur">Rudrapur</option>
									<option value="bareilly">Bareilly</option>
									<option value="shahjahanpur">Shahjahanpur</option>
									<option value="sitapur">Sitapur</option>
									<option value="lucknow">Lucknow</option>
									<option value="ayodhya">Ayodhya</option>
									<option value="gorakhpur">Gorakhpur</option>
									<option value="gopalganj">Gopalganj</option>
									<option value="mirganj">Mirganj</option>
									<option value="siwan">Siwan</option>
									<option value="malmaliya">Malmaliya</option>
									<option value="masrakh">Masrakh</option>
									<option value="mohammadpur mode">Mohammadpur mode</option>
									<option value="pipra kothi">Pipra Kothi</option>
									<option value="muzaffarpur">Muzaffarpur</option>
					            </select>
					        </div>
						</div>

						<div class="form-group select one-fourth">
							<div>
								<select name="drop" required>
									<option value="">To</option>
									<option value="haridwar">Haridwar</option>
									<option value="kashipur">Kashipur</option>
									<option value="rudrapur">Rudrapur</option>
									<option value="bareilly">Bareilly</option>
									<option value="shahjahanpur">Shahjahanpur</option>
									<option value="sitapur">Sitapur</option>
									<option value="lucknow">Lucknow</option>
									<option value="ayodhya">Ayodhya</option>
									<option value="gorakhpur">Gorakhpur</option>
									<option value="gopalganj">Gopalganj</option>
									<option value="mirganj">Mirganj</option>
									<option value="siwan">Siwan</option>
									<option value="malmaliya">Malmaliya</option>
									<option value="masrakh">Masrakh</option>
									<option value="mohammadpur mode">Mohammadpur mode</option>
									<option value="pipra kothi">Pipra Kothi</option>
									<option value="muzaffarpur">Muzaffarpur</option>
								</select>
							</div>
						</div>

						<div class="form-group datepicker one-fourth">
							<input type="date" name="datepick" id="datepick" required="" min="<?= date("Y-m-d", strtotime(' +1 day'));?>" style="height: 45px;">
						</div>
				
						<div class="form-group one-fourth">
							<div class="f-row">
								<button type="submit" class="btn large black">Search</button>
							</div>
						</div>

				    </div>
				</form>
			</div>
		  </div>
		<br/>
	  </div>
   </div>
</div>
		
	<div class="services iconic white">
		<div class="wrap">
			<div class="row">
				<!-- Item -->
				<div class="one-third wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
					<span class="circle"><span class="icon  icon-themeenergy_savings"></span></span>
					<h3>Fixed rates</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  tinc.</p>
				</div>
				<!-- //Item -->
				
				<!-- Item -->
				<div class="one-third wow fadeIn" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">
					<span class="circle"><span class="icon icon-themeenergy_lockpad"></span></span>
					<h3>Reliable transfers</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  tinc.</p>
				</div>
				<!-- //Item -->
				
				<!-- Item -->
				<div class="one-third wow fadeIn" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeIn;">
					<span class="circle"><span class="icon icon-themeenergy_open-wallet"></span></span>
					<h3>No booking fees</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  tinc.</p>
				</div>
				<!-- //Item -->
				
				<!-- Item -->
				<div class="one-third wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
					<span class="circle"><span class="icon icon-themeenergy_heart"></span></span>
					<h3>Free cancellation</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  tinc.</p>
				</div>
				<!-- //Item -->
				
				<!-- Item -->
				<div class="one-third wow fadeIn" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">
					<span class="circle"><span class="icon icon-themeenergy_magic-trick"></span></span>
					<h3>Booking flexibility</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  tinc.</p>
				</div>
				<!-- //Item -->
				
				<!-- Item -->
				<div class="one-third wow fadeIn" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeIn;">
					<span class="circle"><span class="icon icon-themeenergy_call"></span></span>
					<h3>24h customer service</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy  tinc.</p>
				</div>
				<!-- //Item -->
				
			</div>
		</div>
	</div>
	<!-- //Services iconic -->
	
	<!-- Call to action -->
	<div class="black cta">
		<div class="wrap">
			<p class="wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">Like what you see? Are you ready to stand out? You know what to do.</p>
			<a href="#" class="btn huge color right wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">Book Ticket</a>
		</div>
	</div>
	<!-- //Call to action -->
	
	<!-- Services -->
	<div class="services boxed white" id="services">
		<!-- Item -->
		<article class="one-fourth wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
			<figure class="featured-image">
				<img src="./images/img.jpg" alt="">
				<div class="overlay">
					<a href="#" class="expand">+</a>
				</div>
			</figure>
			<div class="details">
				<h4><a href="#">Private transfers</a></h4>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet aliquam erat volutpat.</p>
			</div>
		</article>
		<!-- //Item -->
		
		<!-- Item -->
		<article class="one-fourth wow fadeIn" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">
			<figure class="featured-image">
				<img src="./images/img4.jpg" alt="">
				<div class="overlay">
					<a href="#" class="expand">+</a>
				</div>
			</figure>
			<div class="details">
				<h4><a href="#">Bus transfers</a></h4>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet aliquam erat volutpat.</p>
			</div>
		</article>
		<!-- //Item -->
		
		<!-- Item -->
		<article class="one-fourth wow fadeIn" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeIn;">
			<figure class="featured-image">
				<img src="./images/img2.jpg" alt="">
				<div class="overlay">
					<a href="#" class="expand">+</a>
				</div>
			</figure>
			<div class="details">
				<h4><a href="#">Shuttle transfers</a></h4>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet aliquam erat volutpat.</p>
			</div>
		</article>
		<!-- //Item -->
		
		<!-- Item -->
		<article class="one-fourth wow fadeIn" data-wow-delay=".6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeIn;">
			<figure class="featured-image">
				<img src="./images/img3.jpg" alt="">
				<div class="overlay">
					<a href="#" class="expand">+</a>
				</div>
			</figure>
			<div class="details">
				<h4><a href="#">Helicopter transfers</a></h4>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet aliquam erat volutpat.</p>
			</div>
		</article>
		<!-- //Item -->			
	</div>
	<!-- //Services -->
	
	<!-- Testimonials -->
	<div class="testimonials center black">
		<div class="wrap">
			<h6 class="wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;"><i class="fa fa-quote-left" aria-hidden="true"></i>Wow, this theme is outstanding!</h6>
			<p class="wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation nibh euismod tincidunt ut laoreet aliquam erat volutpat.</p>
			<p class="meta wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">-John Doe, themeforest</p>
		</div>
	</div>
	<!-- //Testimonials -->
	
	<div class="partners white center">
		<div class="wrap">
			<h2 class="wow fadeIn" style="visibility: visible; animation-name: fadeIn;">Our partners</h2>
			<div class="one-fifth wow fadeIn" style="visibility: visible; animation-name: fadeIn;"><a href="#"><img src="./images/logo1.jpg" alt=""></a></div>
			<div class="one-fifth wow fadeIn" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;"><a href="#"><img src="./images/logo2.jpg" alt=""></a></div>
			<div class="one-fifth wow fadeIn" data-wow-delay=".4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeIn;"><a href="#"><img src="./images/logo3.jpg" alt=""></a></div>
			<div class="one-fifth wow fadeIn" data-wow-delay=".6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeIn;"><a href="#"><img src="./images/logo4.jpg" alt=""></a></div>
			<div class="one-fifth" data-wow-delay=".8s"><a href="#"><img src="./images/logo5.jpg" alt=""></a></div>
		</div>
	</div>
	
</main>
	
	