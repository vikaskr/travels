<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\MubUser;

// AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!-- saved from url=(0045) -->
<html class="wf-fontawesome-n4-active wf-active"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="Transfers - Private Transport and Car Hire HTML Template">
  <meta name="description" content="Transfers - Private Transport and Car Hire HTML Template">
  <meta name="author" content="themeenergy.com">
  
  <title>SKT Travels Pvt Ltd</title>
  
  <link rel="stylesheet" href="../css/styler.css">
  <link rel="stylesheet" href="../css/theme-dblue.css" id="template-color">
  <link rel="stylesheet" href="../css/jquery-ui.css">
  <link rel="stylesheet" href="../css/jquery-ui.theme.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/animate.css">
  <link rel="stylesheet" href="../css/icons.css">
  <link rel="shortcut icon" href="images/favicon.ico">
  <script src="../js/webfontloader.js"></script>
  <script src="../js/e808bf9397.js"></script>
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> 
  <link rel="stylesheet" href="../css/e808bf9397.css" media="all">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
  <style type="text/css">
    body
  {
    background: #EEEEEE!important;
    font-family: Montserrat;
  }
.mySlides {display:none;}
  div.selector span:before {
    top: 18px!important;
  }

  [type="date"] {
  background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  97% 50% no-repeat ;
}
[type="date"]::-webkit-inner-spin-button {
  display: none;
}
[type="date"]::-webkit-calendar-picker-indicator {
  opacity: 0;
}
.fa{
  color: #00afef!important;
}
.black {
    background-color: #00afef !important;
}
.copy{
  color: #000!important;
}
.copy a {
    color: #000000!important;
}
.btn.black:hover, .btn.black:focus, .cta .btn:hover, .cta .btn:focus {
    background: #225378 !important;
    color: #fff !important;
}

h3, .h3 {
    font-size: 23px!important;
}

label {
  display: block;
}
div.selector select {
    opacity: 0;
    filter: alpha(opacity=0);
    -moz-opacity: 0;
    border: none;
    background: none;
    position: absolute;
    height: 44px!important;
    top: 0;
    left: 0;
    width: 100%;
}
.form-group div.selector span {
    height: 44px!important;
    line-height: 44px!important;
}
.form-group div.selector {
    border-color: #fff;
    height: 44px!important;
    line-height: 44px!important;
    color: #191F26;
}

input {
  border: 1px solid #c4c4c4;
  border-radius: 3px;
  background-color: #fff;
  padding: 3px 5px;
  width: 100%;
}


</style>
  </head>

<?= Html::csrfMetaTags() ?>
  
    <?php $this->head() ?>
<?php $this->beginBody() ?>
 <body>
 <div class="textwidget">
      <div class="advanced-search color" id="booking">
        <div class="wrap">
        <form role="form" action="search-results.html" method="post">
        <!-- Row -->
        <div class="f-row">

          <div class="form-group one-fifth" style="margin-top: 4px; margin-bottom: 0px!important;">
           <i class="fa fa-map-marker" aria-hidden="true"></i> Haridwar
          </div>

          <div class="form-group one-fifth" style="margin-top: 4px; margin-bottom: 0px!important;">
           <i class="fa fa-phone" aria-hidden="true"></i> +91-9876543210
          </div>

          <div class="form-group one-fifth" style="margin-top: 4px; margin-bottom: 0px!important;">
            <i class="fa fa-whatsapp" aria-hidden="true"></i> +91-9876543210
          </div>

          <div class="form-group one-fifth" style="margin-top: 4px; margin-bottom: 0px!important; text-transform: none!important;">
           <i class="fa fa-envelope" aria-hidden="true"></i> demo@domain.com
          </div>

          <div class="form-group one-fifth" style="margin-top: 0px; margin-bottom: 0px!important;">
             <i class="fa fa-facebook" aria-hidden="true" style=" padding: 6px 9px; background: #fff;  border-radius: 50%;"></i>
             <i class="fa fa-twitter" aria-hidden="true" style=" padding: 6px 7px; background: #fff;  border-radius: 50%;"></i>
             <i class="fa fa-linkedin" aria-hidden="true" style=" padding: 6px 7px; background: #fff;  border-radius: 50%;"></i>
             <i class="fa fa-google-plus" aria-hidden="true" style=" padding: 6px 5px; background: #fff;  border-radius: 50%;"></i>
          </div>
          </div>
      </form>
    </div>
  </div>
</div>


<header class="header" role="banner">
      <div class="wrap">
        <!-- Logo -->
        <div class="logo">
          <a href="/" title="Rama Logistics"><img src="../images/logo.png" alt="Rama Logistics"></a>
        </div>

        <nav role="navigation" class="main-nav">
           <ul>
              <li><a href="/">Home</a></li>
              <li><a href="/site/about">About Us</a></li>
              <li><a href="/site/facilities">Facilities</a></li>
              <li><a href="/site/gallery">Gallery</a></li>
              <li><a href="/site/contact">Contact Us</a></li>
              <!-- <li><a href="/site/cancel">Cancellation</a></li> -->
              <!-- <button class="btn btn-danger" style="margin-top: 22px; margin-left: 20px;"> Booking </button> -->
           </ul>
        </nav>
        <!-- //Main Nav -->
      </div>
</header>
    
    <?= $content ?>

  <footer class="footer black" role="contentinfo">
    <div class="wrap">
      <div class="row">
        <!-- Column -->
        <article class="one-half">
          <h6>About us</h6>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</p>
        </article>
        <!-- //Column -->
        
        <!-- Column -->
        <article class="one-fourth">
          <h6>Need help?</h6>
          <p style="margin: 0px 0px 0px!important">Contact us via phone or email:</p>
          <p class="contact-data" style="margin: 0px 0px 0px!important"><span class="icon icon-themeenergy_call"></span> &nbsp;&nbsp;&nbsp;&nbsp; +1 555 555 555</p>
          <p class="contact-data" style="margin: 0px 0px 0px!important"><span class="icon icon-themeenergy_mail-2"></span> <a href="mailto:help@transfers.com">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;help@transfers.com</a></p>
        </article>
        <!-- //Column -->
        
        <!-- Column -->
        <article class="one-fourth">
          <h6>Follow us</h6>
          <ul class="social">
            <li><a href="#" title="facebook"><i class="fa fa-fw fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#" title="twitter"><i class="fa fa-fw fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#" title="gplus"><i class="fa fa-fw fa-google-plus" aria-hidden="true"></i></a></li>
            <li><a href="#" title="linkedin"><i class="fa fa-fw fa-linkedin" aria-hidden="true"></i></a></li>
          </ul>
        </article>
        <!-- //Column -->
      </div>
      
      <div class="copy">
        <p>© 2018 SKT Travels. All Rights Reserved. Designed By Digitalmazic</p>
        
        <nav role="navigation" class="foot-nav">
          <ul>
            <li><a href="/" title="Home">Home</a></li>
            <li><a href="/site/about" title="About us">About us</a></li>
            <li><a href="/site/contact" title="Contact us">Contact us</a></li>
            <!-- <li><a href="#" title="Terms of use">Terms of use</a></li> -->
            <li><a href="/site/contact" title="Help">Help</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </footer>
  
  <!-- JavaScript -->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/jquery-ui.min.js"></script>
  
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
</script>
  <script src="../js/jquery.uniform.min.js"></script>
  <script src="../js/jquery.slicknav.min.js"></script>
  <script src="../js/wow.min.js"></script>
  <script src="../js/jquery-ui-sliderAccess.js"></script>
  <script src="../js/search.js"></script>
  <script src="../js/scripts.js"></script>
  
  <!-- TEMPLATE STYLES -->

  <script src="../js/styler.js"></script>
  
<!-- END body -->

<div id="ui-datepicker-div" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>

  
<!--REGISTER POPUP END-->
<?php $this->endBody() ?>
<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>

</body>
</html>
<?php $this->endPage() ?>