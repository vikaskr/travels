<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // '/css/bootstrap.min.css',
        '/css/animate.css',
        '/css/stylesheet.css',
        '/css/responsive_style.css',
        '/css/font.css',
        '/css/font-awesome.min.css',
        '/css/fontstyle.css',
        '/css/fontface.css',
        '/css/owl.carousel.css',
        '/css/owl.theme.css',
        '/css/jquery.typeahead.css',
    ];
    public $js = [
        '/js/owl.carousel.js',
        '/js/jqzoom.js',
        '/js/bootstrap.min.js',
        '/js/waypoints.js',
        '/js/jquery_counterup.js',
        '/js/jquery_custom.js',
        '/js/homemap_custom.js',
        'https://maps.googleapis.com/maps/api/js?sensor=false',
        '/js/custom_mub_frontend.js',
        '/js/custom_mub_backend.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\assets\FontAwesomeAsset'
    ];
}
