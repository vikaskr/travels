<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\MubUser;
use app\models\ClientSignup;
use yz\shoppingcart\ShoppingCart;
use app\helpers\PriceHelper;
use yii\helpers\Url;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use app\modules\MubAdmin\modules\csvreader\models\Magazine;
use app\modules\MubAdmin\modules\csvreader\models\Origin;
use app\modules\MubAdmin\modules\csvreader\models\MagazinePrice;

class SiteController extends Controller
{
    public $discount = 0;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authSuccess'],
            ],
        ];
    }


    public function beforeAction($action)
    {
        if (
                $action->id == "payment-process" || $action->id == 'payment-cancel'
        ) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       $this->view->params['page'] = 'home';
      
            return $this->render('index');
            
    }

    public function sendMail($contact)
    {
        $cart = new ShoppingCart();
        $cartItems = ArrayHelper::toArray($cart->getPositions());
        $price = $cart->getCost();
        $success = \Yii::$app->mailer->compose('booking',['cartItems' => $cartItems, 'price' => $price])
          ->setFrom('contact@nursingMagazine.online')
          ->setTo('contact@nursingMagazine.online.test-google-a.com')
          ->setSubject('Your Journal Details')
          ->setTextBody('Plain text content')
          ->setCc($contact->email)
          ->setBcc('praveen@makeubig.com')
          ->send();
        return $success;
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'admin';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack('/mub-admin/');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionSignup()
    {
        return $this->renderAjax('signup');
    }

   public function actionClientLogin()
    {
        if (\Yii::$app->request->isAjax){
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if(\Yii::$app->request->post())
            {
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                   return $this->redirect(Yii::$app->request->referrer);
                }
                else
                {
                    return $this->renderAjax('signin', [
                        'model' => $model,
                    ]);
                }    
            }
            //case the normal request for form
            return $this->renderAjax('signin', [
                'model' => $model,
            ]);
        }
        p('Nice Try!! :D');
    }


    public function actionClientRegister()
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new ClientSignup();
             
            if ($model->load(Yii::$app->request->post())) 
            {      
                if ($user = $model->signup()) 
                {
                    if (Yii::$app->getUser()->login($user))
                    {
                    $mubUserId = \app\models\User::getMubUserId();
                    $mubUserModel = new \app\models\MubUser();
                    $mubUser = $mubUserModel::findOne($mubUserId);
                    $mubUserContact = $mubUser->mubUserContacts;
                    \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                        ->setFrom('info@osmstays.com')
                        ->setTo($mubUserContact->email)
                        ->setCc('info@osmstays.com')
                        ->setSubject('Your Profile Created')
                        ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                        ->send();
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                }
                else
                {
                   $model->addError('username','This Username Already Exists in Database');
                   return $this->renderAjax('signup',['model' => $model]);
                }
            }
            return $model->getErrors();
        }
    }

    public function actionClientRegisterValidate()
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new ClientSignup();
            $model->load(Yii::$app->request->post());
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    public function actionProfile()
    {   
        $this->view->params['page']='profile'; 
        if (Yii::$app->user->isGuest) {
            $this->redirect('/');
        }
        else 
        {
            if(Yii::$app->request->post())
            {
                $postData = \Yii::$app->request->post();
                $userModel = new \app\models\User();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
                $user = $userModel::findOne($mubUser->user_id);
                $mubUserContact = $mubUser->mubUserContacts;
            
                if($mubUser->load($postData) && $mubUserContact->load($postData))
                {
                    if($mubUser->save(false) && $mubUserContact->save(false))
                    {
                        $success = $this->updateUserRecord($postData);
                        if($success)
                        {
                            return $this->goBack('/site/profile');
                        }
                    }
                }
                p([$mubUser->getErrors(),$mubUserContact->getErrors()]);
            }
            return $this->render('profile');
        }
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }

    public function actionLogout()
    {
        $this->layout = false;
        $mubUser = new \app\models\MubUser();
        $mubUserId = \app\models\User::getMubUserId();  
        $currentRole = $mubUser::findOne($mubUserId)['role'];
            if($currentRole == 'client') 
            {
             Yii::$app->user->logout();
                return $this->goHome(); 
            }
            else
            {
            Yii::$app->user->logout();
            return $this->redirect('/');
            }
    }

    
    public function actionSuccesspdf($id)
    {
        $this->layout = false;
        $pdf = \Yii::$app->pdf;
        $pdf->content = $this->renderPartial('successpdf');
        return $pdf->render();
    }

    public function actionGetItems($menuId)
    {
        $this->layout = false;
        $getItem = new Magazine();
        $menuDetail = $getItem::find()->where(['menu_id' => $menuId, 'del_status' => '0'])->all();
        $response = $this->render('items',['items' => $menuDetail]);
       return $response;
    }

    public function actionAddtocart($id)
    {   
        $cart = new ShoppingCart();
        $this->layout = false;
        $model = Magazine::findOne($id);
        if ($model){
            $cart->put($model, 1);
             p($cart->getCost());
            return $this->render('addtocart', [
                'cart' => $cart
            ]);
        }
        throw new NotFoundHttpException();
    }

    public function actionClearCart($id=NULL)
    {
         $cart = new ShoppingCart();
         $this->layout = false;
         if($id == NULL)
         {
            $cart->removeAll();
         }
         else
         {
            $cart->removeById($id);  
         }
         return true;
         // return $this->render('showcart', [
         //    'cart' => $cart]);
    }
    
    

    public function actionCategory($name)
    {
    $this->view->params['page']='category';
    $magazine = new Magazine();
       $userDetails = ip_info();
        $userCountry = $userDetails['country'];
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }
        $userCountry = \app\helpers\StringHelper::generateSlug($userCountry);
        $origin = Origin::find()->where(['origin_slug' => $userCountry])->one();
        
        if(!empty($origin))
        {
            $nationalMagazines = $magazine::find()->where(['del_status' => '0'])->all();
            return $this->render('category',['nationalMagazines' => $nationalMagazines, 'origin' => $origin]);
            
        }
        else
        {
            p("No Country found by name ".$userCountry);
        }
    }

    public function actionAbout()
    {
        $this->view->params['page']='about';
        return $this->render('about');
    }

    public function actionRedirect()
    {
        $this->view->params['page']='redirect';
        return $this->render('redirect');
    }

    public function actionFacilities()
    {
        $this->view->params['page']='facilities';
        return $this->render('facilities');
    }

    public function actionResult()
    {
        $this->view->params['page']='result';
        return $this->render('result');
    }

    public function actionResults()
    {
        $this->view->params['page']='results';
        return $this->render('results');
    }

    public function actionBus()
    {
        $this->view->params['page']='bus';
        return $this->render('bus');
    }

    public function actionGallery()
    {
        $this->view->params['page']='gallery';
        return $this->render('gallery');
    }

    public function actionBusc()
    {
        $this->view->params['page']='busc';
        return $this->render('busc');
    }

    public function actionForm()
    {
        $this->view->params['page']='form';
        return $this->render('form');
    }

     public function actionConfirm()
    {
        $this->view->params['page']='confirm';
        return $this->render('confirm');
    }

    public function actionLayout()
    {
        $this->view->params['page']='layout';
        return $this->render('layout');
    }

    public function actionEmail()
    {
        $this->view->params['page']='email';
        return $this->render('email');
    }

    public function actionSubcriber()
    {
        $subcriber = new \app\models\SignupMail();
        $params = \Yii::$app->request->post();
        if(!empty($params) && $subcriber->load($params))
        {
            $this->layout = false;
            if($subcriber->save(false))
            {   
                return true;
            }
            else
            {
                p($subcriber->getErrors());
            }
                    
        }
        return $this->render('subcriber',['subcriber' => $subcriber]);
    }

    public function actionContact()
    {
        $this->view->params['page']='contact';
        
        return $this->render('contact');
    }

     public function actionBuscheck()
    {
        $this->view->params['page']='buscheck';
        
        return $this->render('buscheck');
    }

    public function actionCancel()
    {
        $this->view->params['page']='cancel';
        
        return $this->render('cancel');
    }


     public function actionCartcount()
    {
        $this->layout = false;
        $this->view->params['page']='cartcount';
        $cart = new ShoppingCart();
        $cartItems = $cart->getPositions();
        $count = count($cartItems);
        return $this->render('cartcount', ['count' => $count]);
    }

    public function actionCheckout()
    {
      $this->view->params['page']='checkout';   
        $userDetails = ip_info();
        $userCountry = $userDetails['country'];
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }
        $userCountry = \app\helpers\StringHelper::generateSlug($userCountry);
        $origin = Origin::find()->where(['origin_slug' => $userCountry])->one();
        $cart = new ShoppingCart();
        $cartItems = $cart->getPositions();
        $count = count($cartItems);
        $originId = $origin->id;
         if(!empty($cartItems)){ 
          foreach($cartItems as $item){
           $magazineId = $item->id;
           $gbpPrice = MagazinePrice::find()->where(['magazine_id' => $magazineId,'origin_id' => $originId])->one();
           $disc = $gbpPrice->discount_percentage;
           $gbp = $gbpPrice->price;
            $discVal = PriceHelper::getCountryPrice($gbp);
            $this->discount += (($disc/100) * ($discVal) * ($item->getQuantity()));
             }
        }
            $discValue = $this->discount;
        if(!empty($origin))
        {
            return $this->render('checkout',['origin' => $origin, 'discValue' => $discValue, 'cartItems' => $cartItems, 'count' => $count, 'cart' => $cart]);
        }
    }


    public function actionShowcart()
    {
        $this->layout = false;
        $cart = new ShoppingCart();
        $items = $cart->getPositions();
        return $this->render('showcart', [
            'items' => $items]);  
    }

    public function actionUpdateCartItems($id,$quantity)
    {  
        if($quantity >= 0)
        {
            $cart = new ShoppingCart();
            $this->layout = false;
            $model = Magazine::findOne($id);
            if($model)
            {
                $cart->update($model, $quantity);
            }
            $total = $cart->getCost(); 
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $data['total'] = $total;
            return $data;
        } 
    }
    
    public function actionForgetpass()
    {
        if(Yii::$app->request->post())
        {
            $postData = \Yii::$app->request->post();
            $mubUserModel = new \app\models\MubUser();
            $mubUserContactModel = new \app\models\MubUserContact();
            $mubUserContact = $mubUserContactModel::find()->where(['email' => $postData['ClientSignup']['email']])->one();
            if(!empty($mubUserContact))
            {  
                $mubUser = $mubUserModel::find()->where(['id' => $mubUserContact->mub_user_id, 'del_status' => '0'])->one();
                $userEmail = $mubUserContact['email'];
                \Yii::$app->mailer->compose('forget',['mubUserContact' => $mubUserContact,'mubUser' => $mubUser])
                    ->setFrom('admin@makeubig.com')
                    ->setTo($mubUserContact->email)
                    ->setCc('praveen@makeubig.com')
                    ->setSubject('Your Magazine World Online Password')->setBcc('admin@makeubig.com','MakeUBIG ADMIN')->send();
                    return 'mailsent';
            }
        }
        return $this->renderAjax('forgetpass');
    }


    public function actionSubsciber()
    {
        $params = \yii::$app->request->getBodyParams();
        $signupMail = new \app\models\SignupMail();
        if($signupMail->load($params))
         {
          if ($signupMail->save())
          {
             \Yii::$app->mailer->compose('subscribe')
                ->setTo($signupMail->email)
                ->setBcc('contact@mazagineworld.online')
                ->setCc('praveen@makeubig.com')
                ->setFrom(['admin@makeubig.com' => 'Magazine'])
                ->setSubject('Customer Signed Up')
                ->setHtmlBody("<b>Subscibe with Mail Id ".$signupMail->email." Signed up to your Newslette Magazines World")
                ->send();    
            return true;
            }
            else
            {
            return false;
            }
        }
    }

}

