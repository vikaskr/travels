<?php

namespace app\migrations;
use app\commands\Migration;

class m170727_125328_seat extends Migration
{
    public function getTableName()
    {
        return 'seat';
    }

    public function getKeyFields()
    {
        return [
                'seat_no' => 'seat_no',
                ];
    }

    public function getFields()
    {
        return [
        'id' => $this->primaryKey(),
        'seat_no' => $this->string(50)->notNull(),
        'date' => $this->string(100)->notNull(),
        'status' => "enum('booked','available') NOT NULL COMMENT '0-Booked,1-Available DEFAULT 1' DEFAULT 'available'",
        'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
        'updated_at' => $this->dateTime(),
        'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
