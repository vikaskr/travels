<?php

namespace app\migrations;
use app\commands\Migration;

class m170607_060232_create_category extends Migration
{
    public function getTableName()
    {
        return 'category';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
        ];
    }

    public function getKeyFields()
    {
        return [
                'category_name' => 'category_name',              
                ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer(),
            'category_name' => $this->string(), 
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
