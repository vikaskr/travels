<?php

namespace app\migrations;
use app\commands\Migration;

class m170607_060230_create_course extends Migration
{
    public function getTableName()
    {
        return 'course';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
        ];
    }

    public function getKeyFields()
    {
        return [
                'name' => 'name',
                ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer(),
            'name' => $this->string(50),
            'slug' => $this->string(50),
            'desciption' => $this->text(),
            'image' => $this->string(), 
            'duration' => $this->string(), 
            'open_date' =>  $this->string(), 
            'no_of_week' =>  $this->string(), 
            'additional_info' => $this->string(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
