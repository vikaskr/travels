<?php

namespace app\migrations;
use app\commands\Migration;

class m170115_144710_create_mub_user_social extends Migration
{
    public function getTableName()
    {
        return 'mub_user_social';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'social_platform_id' => ['mub_social_platforms','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'url' => 'url',
            'mub_user_id'  =>  'mub_user_id',
            'social_platform_id' => 'social_platform_id',
            'platform_username' => 'platform_username'
        ];
    }

    public function getFields()
    {
        return [
        
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->defaultValue(NULL),
            'social_platform_id' => $this->integer(11)->notNull(),
            'platform_username' => $this->string()->notNull()->defaultValue('not_provided'),
            'url' => $this->string()->notNull()->defaultValue('http://www.makeubig.com'),
            'type' =>  $this->string()->notNull()->defaultValue('profile'),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
