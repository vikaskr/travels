<?php

namespace app\migrations;
use app\commands\Migration;

class m170727_125324_price extends Migration
{
    public function getTableName()
    {
        return 'price';
    }

    public function getKeyFields()
    {
        return [
                'price' => 'price',
                ];
    }

    public function getFields()
    {
        return [
        'id' => $this->primaryKey(),
            'price' => $this->string(50)->notNull(),
            'status' => "enum('active','deactivate') NOT NULL COMMENT '0-Active,1-deactivate DEFAULT 0' DEFAULT 'active'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
