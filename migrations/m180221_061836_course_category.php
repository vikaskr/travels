<?php

namespace app\migrations;
use app\commands\Migration;

class m180221_061836_course_category extends Migration
{
   public function getTableName()
    {
        return 'course_category';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'category_id' => ['category','id'],
            'course_id' => ['course','id']
        ];
    }

    public function getKeyFields()
    {
        return [];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}