<?php

namespace app\migrations;
use app\commands\Migration;

class m170607_060228_create_booking extends Migration
{
    public function getTableName()
    {
        return 'booking';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
        ];
    }

    public function getKeyFields()
    {
        return [
                'email' => 'email',
                'mobile' => 'mobile',
               
                ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer(),
            'name' => $this->string(50),
            'email' => $this->string(50),
            'mobile' => $this->string(50), 
            'address' => $this->string(), 
            'address_line_2' => $this->string(),
            'city' =>  $this->string(), 
            'country' => $this->integer(),
            'time' => $this->string(), 
            'amount' => $this->string(),
            'transaction_id' => $this->string(), 
            'status' => "enum('booking','done','cancelled') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT 'booking'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
