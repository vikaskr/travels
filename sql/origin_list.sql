INSERT INTO `origin` (`id`,`continents`, `country_name`,`active`) VALUES
(1, 'AFRICA', 'Algeria', '1'),
(2, 'AFRICA', 'Angola', '1'),
(3, 'AFRICA', 'Benin', '1'),
(4, 'AFRICA', 'Botswana', '1'),
(5, 'AFRICA', 'Burkina Faso', '1'),
(7, 'AFRICA', 'Burundi', '1'),
(8, 'AFRICA', 'Cabo Verde', '1'),
(9, 'AFRICA', 'Cameroon', '1'),
(10, 'AFRICA', 'Central African Republic', '1'),
(11, 'AFRICA', 'Chad', '1'),
(12, 'AFRICA', 'Comoros', '1'),
(13, 'AFRICA', 'Cote d Ivoire', '1'),
(14, 'AFRICA', 'Democratic Republic of the Congo', '1'),
(15, 'AFRICA', 'Djibouti', '1'),
(16, 'AFRICA', 'Egypt', '1'),
(17, 'AFRICA', 'Equatorial Guinea', '1'),
(18, 'AFRICA', 'Eritrea', '1'),
(19, 'AFRICA', 'Ethiopia', '1'),
(20, 'AFRICA', 'Gabon', '1'),
(21, 'AFRICA', 'Gambia', '1'),
(22, 'AFRICA', 'Ghana', '1'),
(23, 'AFRICA', 'Guinea', '1'),
(24, 'AFRICA', 'Guinea-Bissau', '1'),
(25, 'AFRICA', 'Kenya', '1'),
(26, 'AFRICA', 'Lesotho', '1'),
(27, 'AFRICA', 'Liberia', '1'),
(28, 'AFRICA', 'Libya', '1'),
(29, 'AFRICA', 'Madagascar', '1'),
(30, 'AFRICA', 'Malawi', '1'),
(31, 'AFRICA', 'Mali', '1'),
(32, 'AFRICA', 'Mauritania', '1'),
(33, 'AFRICA', 'Mauritius', '1'),
(34, 'AFRICA', 'Morocco', '1'),
(35, 'AFRICA', 'Mozambique', '1'),
(36, 'AFRICA', 'Namibia', '1'),
(37, 'AFRICA', 'Niger', '1'),
(38, 'AFRICA', 'Nigeria', '1'),
(39, 'AFRICA', 'Republic of the Congo', '1'),
(40, 'AFRICA', 'Rwanda', '1'),
(41, 'AFRICA', 'Sao Tome and Principe', '1'),
(42, 'AFRICA', 'Senegal', '1'),
(43, 'AFRICA', 'Seychelles', '1'),
(44, 'AFRICA', 'Sierra Leone', '1'),
(45, 'AFRICA', 'Somalia', '1'),
(46, 'AFRICA', 'South Africa', '1'),
(47, 'AFRICA', 'South Sudan', '1'),
(48, 'AFRICA', 'Sudan', '1'),
(49, 'AFRICA', 'Swaziland', '1'),
(50, 'AFRICA', 'Tanzania', '1'),
(51, 'AFRICA', 'Togo', '1'),
(52, 'AFRICA', 'Tunisia', '1'),
(53, 'AFRICA', 'Uganda', '1'),
(54, 'AFRICA', 'Zambia', '1'),
(55, 'AFRICA', 'Zimbabwe', '1'),
(56, 'ASIA', 'Afghanistan', '1'),
(57, 'ASIA', 'Armenia', '1'),
(58, 'ASIA', 'Azerbaijan', '1'),
(59, 'ASIA', 'Bahrain', '1'),
(60, 'ASIA', 'Bangladesh', '1'),
(61, 'ASIA', 'Bhutan', '1'),
(62, 'ASIA', 'Brunei', '1'),
(63, 'ASIA', 'Cambodia', '1'),
(64, 'ASIA', 'China', '1'),
(65, 'ASIA', 'Cyprus', '1'),
(66, 'ASIA', 'Georgia', '1'),
(67, 'ASIA', 'India', '1'),
(68, 'ASIA', 'Indonesia', '1'),
(69, 'ASIA', 'Iran', '1'),
(70, 'ASIA', 'Iraq', '1'),
(71, 'ASIA', 'Israel', '1'),
(72, 'ASIA', 'Japan', '1'),
(73, 'ASIA', 'Jordan', '1'),
(74, 'ASIA', 'Kazakhstan', '1'),
(75, 'ASIA', 'Kuwait', '1'),
(76, 'ASIA', 'Kyrgyzstan', '1'),
(77, 'ASIA', 'Laos', '1'),
(78, 'ASIA', 'Lebanon', '1'),
(79, 'ASIA', 'Malaysia', '1'),
(80, 'ASIA', 'Maldives', '1'),
(81, 'ASIA', 'Mongolia', '1'),
(82, 'ASIA', 'Myanmar', '1'),
(83, 'ASIA', 'Nepal', '1'),
(84, 'ASIA', 'North Korea', '1'),
(85, 'ASIA', 'Oman', '1'),
(86, 'ASIA', 'Pakistan', '1'),
(87, 'ASIA', 'Palestine', '1'),
(88, 'ASIA', 'Philippines', '1'),
(89, 'ASIA', 'Qatar', '1'),
(90, 'ASIA', 'Russia', '1'),
(91, 'ASIA', 'Saudi Arabia', '1'),
(92, 'ASIA', 'Singapore', '1'),
(93, 'ASIA', 'South Korea', '1'),
(94, 'ASIA', 'Sri Lanka', '1'),
(95, 'ASIA', 'Syria', '1'),
(96, 'ASIA', 'Taiwan', '1'),
(97, 'ASIA', 'Tajikistan', '1'),
(98, 'ASIA', 'Thailand', '1'),
(99, 'ASIA', 'Timor-Leste', '1'),
(100, 'ASIA', 'Turkey', '1'),
(101, 'ASIA', 'Turkmenistan', '1'),
(102, 'ASIA', 'United Arab Emirates', '1'),
(103, 'ASIA', 'Uzbekistan', '1'),
(104, 'ASIA', 'Vietnam', '1'),
(105, 'ASIA', 'Yemen', '1'),
(106, 'AUSTRALIA', 'Australia', '1'),
(107, 'AUSTRALIA', 'Fiji', '1'),
(108, 'AUSTRALIA', 'Kiribati', '1'),
(109, 'AUSTRALIA', 'Marshall Islands', '1'),
(110, 'AUSTRALIA', 'Micronesia', '1'),
(111, 'AUSTRALIA', 'Nauru', '1'),
(112, 'AUSTRALIA', 'New Zealand', '1'),
(113, 'AUSTRALIA', 'Palau', '1'),
(114, 'AUSTRALIA', 'Papua New Guinea', '1'),
(115, 'AUSTRALIA', 'Samoa', '1'),
(116, 'AUSTRALIA', 'Solomon Islands', '1'),
(117, 'EUROPE',	'Albania', '1'),
(118, 'EUROPE',	'Andorra', '1'),
(119, 'EUROPE',	'Armenia', '1'),
(120, 'EUROPE',	'Austria', '1'),
(121, 'EUROPE',	'Azerbaijan', '1'),
(122, 'EUROPE',	'Belarus', '1'),
(123, 'EUROPE',	'Belgium', '1'),
(124, 'EUROPE',	'Bosnia and Herzegovina', '1'),
(125, 'EUROPE',	'Bulgaria', '1'),
(126, 'EUROPE',	'Croatia', '1'),
(127, 'EUROPE',	'Cyprus', '1'),
(128, 'EUROPE',	'Czech Republic', '1'),
(129, 'EUROPE',	'Denmark', '1'),
(130, 'EUROPE',	'Estonia', '1'),
(131, 'EUROPE',	'Finland', '1'),
(132, 'EUROPE',	'France', '1'),
(133, 'EUROPE',	'Georgia', '1'),
(134, 'EUROPE',	'Germany', '1'),
(135, 'EUROPE',	'Greece', '1'),
(136, 'EUROPE',	'Hungary', '1'),
(137, 'EUROPE',	'Iceland', '1'),
(138, 'EUROPE',	'Ireland', '1'),
(139, 'EUROPE',	'Italy', '1'),
(140, 'EUROPE',	'Kazakhstan', '1'),
(141, 'EUROPE',	'Kosovo', '1'),
(142, 'EUROPE',	'Latvia', '1'),
(143, 'EUROPE',	'Liechtenstein', '1'),
(144, 'EUROPE',	'Lithuania', '1'),
(145, 'EUROPE',	'Luxembourg', '1'),
(146, 'EUROPE',	'Macedonia (FYROM)', '1'),
(147, 'EUROPE',	'Malta', '1'),
(148, 'EUROPE',	'Moldova', '1'),
(149, 'EUROPE',	'Monaco', '1'),
(150, 'EUROPE',	'Montenegro', '1'),
(151, 'EUROPE',	'Netherlands', '1'),
(152, 'EUROPE',	'Norway', '1'),
(153, 'EUROPE',	'Poland', '1'),
(154, 'EUROPE',	'Portugal', '1'),
(155, 'EUROPE',	'Romania', '1'),
(156, 'EUROPE',	'Russia', '1'),
(157, 'EUROPE',	'San Marino', '1'),
(158, 'EUROPE',	'Serbia', '1'),
(159, 'EUROPE',	'Slovakia', '1'),
(160, 'EUROPE',	'Slovenia', '1'),
(161, 'EUROPE',	'Spain', '1'),
(162, 'EUROPE',	'Sweden', '1'),
(163, 'EUROPE',	'Switzerland', '1'),
(164, 'EUROPE',	'Turkey', '1'),
(165, 'EUROPE',	'Ukraine', '1'),
(166, 'EUROPE',	'United Kingdom', '1'),
(167, 'EUROPE',	'Vatican City', '1'),
(168, 'NORTH AMERICA', 'Antigua and Barbuda', '1'),
(169, 'NORTH AMERICA', 'Bahamas', '1'),
(170, 'NORTH AMERICA', 'Barbados', '1'),
(171, 'NORTH AMERICA', 'Belize', '1'),
(172, 'NORTH AMERICA', 'Canada', '1'),
(173, 'NORTH AMERICA', 'Costa Rica', '1'),
(174, 'NORTH AMERICA', 'Cuba', '1'),
(175, 'NORTH AMERICA', 'Dominica', '1'),
(176, 'NORTH AMERICA', 'Dominican Republic', '1'),
(177, 'NORTH AMERICA', 'El Salvador', '1'),
(178, 'NORTH AMERICA', 'Grenada', '1'),
(179, 'NORTH AMERICA', 'Guatemala', '1'),
(180, 'NORTH AMERICA', 'Haiti', '1'),
(181, 'NORTH AMERICA', 'Honduras', '1'),
(182, 'NORTH AMERICA', 'Jamaica', '1'),
(183, 'NORTH AMERICA', 'Mexico', '1'),
(184, 'NORTH AMERICA', 'Nicaragua', '1'),
(185, 'NORTH AMERICA', 'Panama', '1'),
(186, 'NORTH AMERICA', 'Saint Kitts and Nevis', '1'),
(187, 'NORTH AMERICA', 'Saint Lucia', '1'),
(188, 'NORTH AMERICA', 'Saint Vincent and the Grenadines', '1'),
(189, 'NORTH AMERICA', 'Trinidad and Tobago', '1'),
(190, 'NORTH AMERICA', 'United States of America', '1'),
(191, 'SOUTH AMERICA', 'Argentina', '1'),
(192, 'SOUTH AMERICA', 'Bolivia', '1'),
(193, 'SOUTH AMERICA', 'Brazil', '1'),
(194, 'SOUTH AMERICA', 'Chile', '1'),
(195, 'SOUTH AMERICA', 'Colombia', '1'),
(196, 'SOUTH AMERICA', 'Ecuador', '1'),
(197, 'SOUTH AMERICA', 'Guyana', '1'),
(198, 'SOUTH AMERICA', 'Paraguay', '1'),
(199, 'SOUTH AMERICA', 'Peru', '1'),
(200, 'SOUTH AMERICA', 'Suriname', '1'),
(201, 'SOUTH AMERICA', 'Urugua', '1'),
(202, 'SOUTH AMERICA', 'Venezuela2', '1');