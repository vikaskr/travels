<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id
 * @property integer $origin_id
 * @property string $name
 * @property string $slug
 * @property string $code
 * @property string $symbol
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Origin $origin
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['origin_id'], 'required'],
            [['origin_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['status', 'del_status'], 'string'],
            [['name', 'slug'], 'string', 'max' => 100],
            [['code', 'symbol'], 'string', 'max' => 255],
            [['origin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Origin::className(), 'targetAttribute' => ['origin_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => 'Origin ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'code' => 'Code',
            'symbol' => 'Symbol',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrigin()
    {
        return $this->hasOne(Origin::className(), ['id' => 'origin_id']);
    }
}
