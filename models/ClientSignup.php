<?php
namespace app\models;

use yii\base\Model;

class ClientSignup extends Model
{
    public $first_name;
    public $last_name;
    public $username;
    public $password;
    public $gender;
    public $dob;
    public $email;
    public $password_confirm;
    public $mobile;
    public $address;
    public $address_line_2;
    public $name_on_certificate;
    public $course_location;
    public $courses;
    public $other_courses;
    public $get_to_know;
    public $lat;
    public $country;
    public $long;
    public $domain;
    public $organization;
    public $city;
    public $state;
    public $user_states;
    public $modelClass = '\app\models\MubUserContact';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username','first_name','last_name'], 'trim'],

            [['username','first_name','last_name','email','mobile','address','country'], 'required'],

            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['email','email','message' => 'This is not a valid email format'],
            [['username','first_name','last_name','address','organization','lat','long','address_line_2','name_on_certificate','course_location','courses','other_courses','get_to_know','country'], 'string', 'min' => 2, 'max' => 255],
            ['password', 'required'],
            ['mobile','number'],
            ['mobile','string', 'max' => 13 , 'min' => 10],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        $this->username = $this->randomstring(10);
        $this->password = $this->randomstring(10);
        if (!$this->validate()) {
             return false;
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {
        $user = new User();
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->username = strtolower(str_replace(' ', '_', $this->username));
        $user->password = $this->password;
        $user->dob = ($this->dob) ? $this->dob : '1970-01-01 12:00:00';
        $user->gender = ($this->gender) ? $this->gender : 'Male';
        $user->status = 'Active';
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        if($user->save())
        {
            $auth = \Yii::$app->authManager;
            $client = $auth->createRole('client');
            $auth->assign($client, $user->id);
            $mubUser = new \app\models\MubUser();
            $mubUserContact = new \app\models\MubUserContact();
            $mubUser->user_id = $user->id;
            $mubUser->first_name = $this->first_name;
            $mubUser->last_name = $this->last_name;
            $mubUser->role = 'client';
            $mubUser->username = strtolower(str_replace(' ', '_', $this->username));
            $mubUser->password = $this->password;
            $mubUser->gender = ($this->gender) ? $this->gender : 'Male';
            $mubUser->dob = ($this->dob) ? $this->dob : '1970-01-01 12:00:00';
            $mubUser->domain = ($this->domain) ? $this->domain : 'www.yourwebsite.com';
            $mubUser->organization = ($this->organization) ? $this->organization : 'Your Company';
            $mubUser->status = 'Active';
            if($mubUser->save(false))
            {
                $mubUserContact->mub_user_id = $mubUser->id;
                $mubUserContact->city = ($this->city) ? $this->city: '125';
                $mubUserContact->state = ($this->state) ? $this->state: '10';
                $mubUserContact->pin_code = '1100089';
                $mubUserContact->landline = '023456789';
                $mubUserContact->email = ($this->email) ? $this->email: 'email@company.com';
                $mubUserContact->mobile = ($this->mobile) ? $this->mobile : '0987654321';
                $mubUserContact->address = ($this->address) ? $this->address : 'Your complete address'; 
                $mubUserContact->lat = ($this->lat) ? $this->lat : '78.784842'; 
                $mubUserContact->long = ($this->long) ? $this->long : '26.954844'; 
                $mubUserContact->address_line_2 = ($this->address_line_2) ? $this->address_line_2 : '09876 jsdnvjs nd vns 54321';
                $mubUserContact->name_on_certificate = ($this->name_on_certificate) ? $this->name_on_certificate : 'ksdfv';
                $mubUserContact->course_location = ($this->course_location) ? $this->course_location : 'jsdnjvcn ';
                $mubUserContact->courses = ($this->courses) ? $this->courses : 'sjdnv';
                $mubUserContact->other_courses = ($this->other_courses) ? $this->other_courses : 'jsdfn jmnsk';
                $mubUserContact->get_to_know = ($this->get_to_know) ? $this->get_to_know : 'jsdnjkn';
                $mubUserContact->country = $this->country;
                
                if($mubUserContact->save(false))
                {
                    $transaction->commit();
                    return $user;
                }
                else
                {
                    $errors = implode(',', $mubUserContact->getErrors());
                throw new yii\web\ForbiddenHttpException('User Not Created Because : '. $errors);
                }    
            }
            else
            {
                $errors = implode(',', $mubUser->getErrors());
                throw new yii\web\ForbiddenHttpException('User Not Created Because : '. $errors);
            }
        }else
        {
            p($user->getErrors());
        }
        $errors = implode(',', $user->getErrors());
        throw new yii\web\ForbiddenHttpException('User Not Created Because : '. $errors);
        }
        catch (\Exception $e) {
                    $transaction->rollBack();
                    p($e);
                }
          return null;
    }

    function randomstring($len)
    {
    $string = "";
    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for($i=0;$i<$len;$i++)
    $string.=substr($chars,rand(0,strlen($chars)),1);
    return $string;
    }
}
