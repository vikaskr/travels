<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mub_user_contact".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $email
 * @property integer $city
 * @property string $pin_code
 * @property string $mobile
 * @property string $landline
 * @property string $work_phone
 * @property string $address
 * @property string $lat
 * @property string $address_line_2
 * @property string $name_on_certificate
 * @property string $course_location
 * @property string $courses
 * @property string $other_courses
 * @property string $get_to_know
 * @property string $long
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property City $city0
 * @property MubUser $mubUser
 */
class MubUserContact extends \app\components\Model
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'mub_user_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id'],'integer'],
            [['email', 'city','mobile','address','city','state','country'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['status', 'del_status','address_line_2','name_on_certificate','course_location','courses','other_courses','get_to_know'], 'string'],
            [['email'], 'email'],
            ['mobile','string', 'max' => 10 , 'min' => 10],
            [['pin_code', 'mobile', 'landline', 'work_phone'], 'string', 'max' => 12],
            [['address'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mub_user_id' => Yii::t('app', 'Mub User ID'),
            'email' => Yii::t('app', 'Email'),
            'city' => Yii::t('app', 'City'),
            'pin_code' => Yii::t('app', 'Pin Code'),
            'country' => Yii::t('app', 'Country'),
            'mobile' => Yii::t('app', 'Mobile'),
            'landline' => Yii::t('app', 'Landline'),
            'work_phone' => Yii::t('app', 'Work Phone'),
            'address' => Yii::t('app', 'Address Line 1'),
            'address_line_2' => Yii::t('app', 'Address Line 2'),
            'name_on_certificate' => Yii::t('app', 'Name On Certificate'),
            'course_location' =>  Yii::t('app', 'Course Location'),
            'courses' =>  Yii::t('app', 'Courses'),
            'other_courses' =>  Yii::t('app', 'Other Courses'),
            'get_to_know' => Yii::t('app', 'Get To know'),
            'lat' => Yii::t('app', 'Lat'),
            'long' => Yii::t('app', 'Long'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id'])->where(['del_status' => '0']);
    }
}
