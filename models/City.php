<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $city_name
 * @property integer $state_id
 * @property string $region_id
 * @property integer $order_by
 * @property integer $pin_code
 *
 * @property State $state
 * @property MubUserContact[] $mubUserContacts
 */
class City extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state_id', 'order_by', 'pin_code'], 'integer'],
            [['city_name'], 'string', 'max' => 100],
            [['region_id'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_name' => Yii::t('app', 'City Name'),
            'state_id' => Yii::t('app', 'State ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'order_by' => Yii::t('app', 'Order By'),
            'pin_code' => Yii::t('app', 'Pin Code'),
        ];
    }
}
