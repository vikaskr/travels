<?php

namespace app\modules\MubAdmin\modules\yoga\models;

use Yii;
use app\models\MubUser;

/**
 * This is the model class for table "courses".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $name
 * @property string $slug
 * @property string $desciption
 * @property string $image
 * @property string $duration
 * @property string $open_date
 * @property string $no_of_week
 * @property string $additional_info
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 */
class Courses extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id'], 'integer'],
            [['desciption', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 50],
            [['image', 'duration', 'open_date', 'no_of_week', 'additional_info'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'desciption' => 'Desciption',
            'image' => 'Image',
            'duration' => 'Duration',
            'open_date' => 'Booking Date(yyyy/mm/dd) Use " , " for multiple dates' ,
            'no_of_week' => 'No Of Week',
            'additional_info' => 'Additional Info',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
