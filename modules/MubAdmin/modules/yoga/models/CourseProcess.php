<?php 
namespace app\modules\MubAdmin\modules\yoga\models;

use app\components\Model;

class CourseProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $course = new Course();
        $this->models = [
            'course' => $course
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $course = $model;
        $this->relatedModels = [
            'course' => $course
        ];
        return $this->relatedModels;
    }

    public function saveCourse($course)
    {
        $userId = \app\models\User::getMubUserId();
        $course->mub_user_id =  $userId;
        return ($course->save()) ? $course->id : p($course->getErrors());
    }

    public function saveData($data)
    {
        if(isset($data['course']))
        {
        try {
            $catId = $this->saveCourse($data['course']);
            return ($catId) ? $catId : false;  
            }
            catch (\Exception $e)
            {
                throw $e;
            }
        }
        throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
