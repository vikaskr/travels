<?php 
namespace app\modules\MubAdmin\modules\yoga\models;

use app\components\Model;

class CoursesProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $courses = new Courses();
        $this->models = [
            'courses' => $courses
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $courses = $model;
        $this->relatedModels = [
            'courses' => $courses
        ];
        return $this->relatedModels;
    }

    public function saveCourses($courses)
    {
        $userId = \app\models\User::getMubUserId();
        $courses->mub_user_id =  $userId;
        return ($courses->save()) ? $courses->id : p($courses->getErrors());
    }

    public function saveData($data)
    {
        if(isset($data['courses']))
        {
        try {
            $catId = $this->saveCourses($data['courses']);
            return ($catId) ? $catId : false;  
            }
            catch (\Exception $e)
            {
                throw $e;
            }
        }
        throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
