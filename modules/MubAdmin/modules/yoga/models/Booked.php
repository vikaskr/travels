<?php

namespace app\modules\MubAdmin\modules\yoga\models;

use Yii;

/**
 * This is the model class for table "booked".
 *
 * @property integer $id
 * @property string $date_booked
 * @property string $seat
 * @property string $froms
 */
class Booked extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booked';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_booked', 'seat', 'froms'], 'required'],
            [['date_booked', 'seat', 'froms'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_booked' => 'Date Booked',
            'seat' => 'Seat',
            'froms' => 'Froms',
        ];
    }
}
