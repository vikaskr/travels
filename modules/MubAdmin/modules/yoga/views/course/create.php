<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\yoga\models\Course */

$this->title = 'Delhi to Shimla';
$this->params['breadcrumbs'][] = ['label' => 'Seats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'course' => $course,
    ]) ?>

</div>
