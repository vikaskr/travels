<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $course app\modules\MubAdmin\modules\yoga\courses\CourseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($courses, 'id') ?>

    <?= $form->field($courses, 'mub_user_id') ?>

    <?= $form->field($courses, 'name') ?>

  

    <?php // echo $form->field($courses, 'duration') ?>

    <?php // echo $form->field($courses, 'open_date') ?>

    <?php // echo $form->field($courses, 'no_of_week') ?>

    <?php // echo $form->field($courses, 'additional_info') ?>

    <?php  echo $form->field($courses, 'status') ?>

    <?php // echo $form->field($courses, 'created_at') ?>

    <?php // echo $form->field($courses, 'updated_at') ?>

    <?php  echo $form->field($courses, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
