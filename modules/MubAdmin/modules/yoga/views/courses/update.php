<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $course app\modules\MubAdmin\modules\yoga\courses\Course */

$this->title = 'Update Course: ' . $courses->name;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $courses->name, 'url' => ['view', 'id' => $courses->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="course-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'courses' => $courses,
    ]) ?>

</div>
