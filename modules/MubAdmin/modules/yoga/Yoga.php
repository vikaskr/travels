<?php

namespace app\modules\MubAdmin\modules\Yoga;

class Yoga extends \app\modules\MubAdmin\MubAdmin
{
    public $controllerNamespace = 'app\modules\MubAdmin\modules\yoga\controllers';

    public $defaultRoute = 'course';
    
    public function init()
    {
        parent::init();
    }
}
