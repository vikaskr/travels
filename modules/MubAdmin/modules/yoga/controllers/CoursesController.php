<?php

namespace app\modules\MubAdmin\modules\yoga\controllers;

use Yii;
use app\modules\MubAdmin\modules\yoga\models\Courses;
use app\modules\MubAdmin\modules\yoga\models\CoursesSearch;
use app\modules\MubAdmin\modules\yoga\models\CoursesProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CoursesController implements the CRUD actions for Courses model.
 */
class CoursesController extends MubController
{
   public function getPrimaryModel()
   {
        return new Courses();
   }

   public function getProcessModel()
   {
        return new CoursesProcess();
   }

   public function getSearchModel()
   {
        return new CoursesSearch();
   }
}