<?php

namespace app\modules\MubAdmin\modules\yoga\controllers;

use Yii;
use app\components\MubController;
use app\modules\MubAdmin\modules\yoga\models\Course;
use app\modules\MubAdmin\modules\yoga\models\CourseSearch;
use app\modules\MubAdmin\modules\yoga\models\CourseProcess;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CourseController extends MubController
{
   public function getPrimaryModel()
   {
        return new Course();
   }

   public function getProcessModel()
   {
        return new CourseProcess();
   }

   public function getSearchModel()
   {
        return new CourseSearch();
   }
}
