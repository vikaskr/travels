<?php 

namespace app\modules\MubAdmin\modules\csvreader\models;
use app\modules\MubAdmin\modules\csvreader\models\Magazine;
use app\modules\MubAdmin\modules\csvreader\models\Subject;
use app\modules\MubAdmin\modules\csvreader\models\Origin;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;

class CsvProcess extends Model
{
	  public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $this->models = [''];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [];
        return $this->relatedModels;
    }


    public function saveMagazine($data)
    {
      $magazineModel = new Magazine();
      if($magazineModel->load($data) && $magazineModel->validate())
        { 
        $slug = $data['Magazine']['slug'];
        $exists = $magazineModel::find()->where(['slug' => $slug])->one();
        if($exists)
        { 
          $response = $magazineModel->updateAll($data['Magazine'],['magazine.id' => $exists->id]);
          return $exists->id;
        }
        else
        { 
          if(!$magazineModel->save())
          {   
              p($magazineModel->getErrors());
          }
          return $magazineModel->id;
        }
      }
      else
      { 
        p($magazineModel->getErrors());
      }
    }

    public function saveMagazinePrice($magazineId,$data,$countries)
    {      
      $origin = new Origin();
      $values = '';
      $totalCountries = count($countries);
      $i = 0;
      foreach ($countries as $originSlug => $index) 
      {
        $magOrigin = $origin::find()->where(['origin_slug' => $originSlug,'del_status' => '0'])->one();
        if(empty($magOrigin))
        {
          p('No record for country '. $originSlug);
        }
        $price = $data[$index];
        $discount_percentage = ($data[$index+1]=='') ?0 :$data[$index+1];
        $featured = $data[$index+2];

        $values .= "('".$magazineId."','".$magOrigin->id."','".$price."','".$discount_percentage."','".$featured."')";
        if($i != $totalCountries-1)
        {
          $values .= ',';
        }
        $i++;
      }
      $command = "INSERT INTO `magazine_price` (`magazine_id`,`origin_id`,`price`,`discount_percentage`,`featured`) VALUES ".$values." ON DUPLICATE KEY UPDATE `magazine_id`=`magazine_id`,`origin_id`=`origin_id`,`price`=`price`,`discount_percentage`=`discount_percentage`,`featured`=`featured`";
      $sql = \Yii::$app->db->createCommand($command)->execute();
      return $sql;
    }


    public function saveData($csvFile)
    {
      ini_set('max_execution_time', 200000);
      ini_set('memory_limit', '-1');
      $publish = new Publisher();
      $lang = new Language();
      $org = new Origin();
      $sub = new Subject();
      $countries = [];
      $columns = [];
        if(!empty($csvFile))
        {
           foreach ($csvFile as $rowKey => $row) 
           {
               $dataSet = explode(';',$row);
               if($rowKey == 0)
               {
                $flag = 0;
                  foreach ($dataSet as $dataKey => $name) 
                  {
                      if($name == 'BASE PRICE CURRENCY')
                      {
                          $flag = 1;
                          continue;
                      }
                      if($flag == 1)
                      {
                        if(trim($name) != 'Discount') 
                        {
                          if(trim($name) != 'Featured') 
                          {
                          $nameSlug = \app\helpers\StringHelper::generateSlug($name);
                          $countries[$nameSlug] = $dataKey; 
                          }
                        }
                      }
                  }
               }
               else
               {
                //Saving Subject and getting Subject Id
               $slugSubject = $dataSet[1];
               $subjectSlug = \app\helpers\StringHelper::generateSlug($slugSubject);
               $subject = $sub::find()->where(['subject_slug' => $subjectSlug])->one();

               if(empty($subject))
               {
                  p('Subject Not Found '.$subjectSlug);
              }
               else
               {
                $subjectId = $subject->id;
               }
               //Saving Publisher and getting Publisher Id
               $slugPublish = (isset($dataSet[7]) && ($dataSet[7] !== ''))? $dataSet[7] : 'NA';
               $publisherSlug = \app\helpers\StringHelper::generateSlug($slugPublish);
               $publisher = $publish::find()->where(['slug' => $publisherSlug])->one();

               if(empty($publisher))
               {
                  $publish->slug = $publisherSlug;
                  $publish->publisher = $dataSet[7];
                  if(!$publish->save())
                  {
                    p($publish->getErrors());
                  }
                  $publisherId = $publish->id;
               }
               else
               {
                $publisherId = $publisher->id;
               }
               //Saving Language and getting Language Id
               $sluglang = $dataSet[9];
               $languageSlug = \app\helpers\StringHelper::generateSlug($sluglang);
               $language = $lang::find()->where(['slug' => $languageSlug])->one();
               if(empty($language))
               {
                  $lang->slug = $languageSlug;
                  $lang->language = $dataSet[9];
                  if(!$lang->save())
                  {
                    p($lang->getErrors());
                  }
                  $languageId = $lang->id;
               }
               else
               {
                $languageId = $language->id;
               }

               //getting the origin Id of the Magazine
               $slugOrg = $dataSet[8];
               $originSlug = \app\helpers\StringHelper::generateSlug($slugOrg);
               $origin = $org::find()->where(['origin_slug' => $originSlug])->one();
               if(empty($origin))
               {
                  $org->origin_slug = $originSlug;
                  $org->origin_name = $dataSet[8];
                  if(!$org->save())
                  {
                    p($org->getErrors());
                  }
                  $originId = $org->id;
               }
               else
               {
                $originId = $origin->id;
               }

               if($dataSet[0] != '')
               {
                 $pubMask = (isset($dataSet[16])) ? $dataSet[16] : 'NA';
                 $slugMask = $dataSet[3];
                 $data['Magazine']['product_code'] = (isset($dataSet[0]) && ($dataSet[0] !== ''))? $dataSet[0] : 'NA';
                 $data['Magazine']['subject_id'] = $subjectId;
                 $data['Magazine']['cover_image'] = (isset($dataSet[2]) && ($dataSet[2] !== ''))? $dataSet[2] : 'NA';
                 $data['Magazine']['name'] = (isset($dataSet[3]) && ($dataSet[3] !== ''))? $dataSet[3] : 'NA';
                 $data['Magazine']['slug'] = \app\helpers\StringHelper::generateSlug($slugMask);
                 $data['Magazine']['issues_per_year'] = (isset($dataSet[4]) && ($dataSet[4] !== ''))?$dataSet[4] :'NA';
                 $data['Magazine']['issn'] = (isset($dataSet[5]) && ($dataSet[5] !== ''))?$dataSet[5] :'NA';
                 $data['Magazine']['periodicity'] =  (isset($dataSet[6]) && ($dataSet[6] !== ''))?$dataSet[6] :'NA';
                 $data['Magazine']['publisher_id'] = $publisherId;
                 $data['Magazine']['language_id'] = $languageId;
                 $data['Magazine']['origin_id'] = $originId;
                 $data['Magazine']['estimated_delivery'] = (isset($dataSet[10]) && ($dataSet[10] !== ''))?$dataSet[10] :'NA';
                 $data['Magazine']['format'] = (isset($dataSet[11]) && ($dataSet[11] !== ''))?$dataSet[11] :'NA';
                 $data['Magazine']['promoted_by'] = (isset($dataSet[12]) && ($dataSet[12] !== ''))?$dataSet[12] :'NA';
                 $data['Magazine']['distributed_by'] = (isset($dataSet[13]) && ($dataSet[13] !== ''))?$dataSet[13] :'NA';
                 $data['Magazine']['supplied_by'] = (isset($dataSet[14]) && ($dataSet[14] !== ''))?$dataSet[14] :'NA';
                 $data['Magazine']['marketed_by'] = (isset($dataSet[15]) && ($dataSet[15] !== ''))?$dataSet[15] :'NA';
                 $data['Magazine']['description'] =  (isset($dataSet[16]) && ($dataSet[16] !== ''))?$dataSet[16] :'NA';
                 if($data['Magazine']['name'] != 'NA')
                 {
                   $magazineId = $this->saveMagazine($data);
                   if($magazineId)
                   {
                     $this->saveMagazinePrice($magazineId,$dataSet,$countries);
                   }
                 }
               }
           }
         }
        }
        return true;
    }
}