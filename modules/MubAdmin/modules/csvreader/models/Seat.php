<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;

/**
 * This is the model class for table "seat".
 *
 * @property integer $id
 * @property string $seat_no
 * @property string $date
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 */
class Seat extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seat_no', 'date'], 'required'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['seat_no'], 'string', 'max' => 50],
            [['date'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seat_no' => 'Seat No',
            'date' => 'Date',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }
}
