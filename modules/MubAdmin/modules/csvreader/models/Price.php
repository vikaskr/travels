<?php
use yii\base\Model;
namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;
use app\components\Model;
use app\helpers\HtmlHelper;
/**
 * This is the model class for table "price".
 *
 * @property integer $id
 * @property string $price
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 */
class Price extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price'], 'required'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['price'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }
}
