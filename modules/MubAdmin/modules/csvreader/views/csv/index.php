<?php
use \ruskid\csvimporter\CSVImporter;
use \ruskid\csvimporter\CSVReader;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="x_panel">
<?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
    <?= $form->field($model,'file')->fileInput() ?>
    <div class="form-group">
        <?= Html::submitButton('Save',['class'=>'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
</div>