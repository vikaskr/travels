<?php
 
    $name = $_GET['name'];
    $email = $_GET['email'];
    $phone = $_GET['phone'];
    $date = $_GET['date'];
    $pick = $_GET['pick'];
    $drop = $_GET['drop'];
    $seat = $_GET['seat'];

    $redirect = "/mub-admin/dashboard/passenger"; 
    $to      = $email;
    $subject = 'Booking Confirm From SKT Travels';
    $body = "Name: " . $name . "\n" . "Email: " . $email . "\n" . "Phone: " . $phone . "\n" . "Journey Date: " . $date . "\n" . "From: " . $pick . "\n" . "To: " . $drop . "\n" . "Seats: " . $seat . "\n";
 
    $headers = 'From: admin@demowala.in' . "\r\n" .
        'Reply-To: vikas@digitalmazic.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    mail($to,$subject,$body,$headers);
    header('Location: '.$redirect);
?>